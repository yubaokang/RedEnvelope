package com.alipay;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class AliPayUtils {

    // 商户PID------签约的支付宝账号对应的支付宝唯一用户号。以2088开头的16位纯数字组成。
    //  public String PARTNER = "2088402110697628";//我
    // 商户收款账号
    // 商户私钥，pkcs8格式
    //  public String RSA_PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJ3q4i9QzX1/MIIc/q5r14T29Zn5WXKnbjoDypJdHpAfMThwTQxhOeT13uUxD83Aqia08TArV+bBl+kF76CH+MFhZ2G9ZkjKNrFgPyUi2VH0ASZT/mx+GgP4dqbipD80YijtGLtre4yPmk/9LayERrGDAkEuVbavuLzPFxRk6WYzAgMBAAECgYBUl/WQSSW7G349xRB9GRH7jVulnthWvfAjLdK/MxjjNYJlbGspvY+I4lkDx5Nisdm0IGuvVRiiaYRZ9K8X/J65CWOFO2YtV/de7n1bdR3BjfeHvbzrBIy10+a0p4heYlclLrkzmcUnDHZyu2dagjM6hDhavCK5fIDGmI/PtbhjEQJBAMzTI3p02hAthV8t5F6naUhYmfFDdiSSL66sLlWh7/ZcY1A5QWq7ZtWuBWUJDo29u+u24hxE2k5M/ezdgW/aDvkCQQDFX37l95p6RPlp0JSoRbk+1QKBt2uQqtXaADG6r5b7p31sSng5UP1S+nN1sA/9vcyXlkhHjnuXns+M/F7Qza2LAkBjRq86imqrTrUHVgILks8n3rH2y4D25tgvrIrkvJHHci480xOO/Pgx95/rwcEJGH5+GG1t0D2TmCO3bAFxylpZAkEAgO8NOrUxGMaOF+3F7tb9WGpWJN7pSawP30VZNxcVjnevm1pahza9BcPOQQcuqxed2zi1JumRG08HTpZeahCyWQJAYB/mUOzPXjq1x/emXVgXD1jKbW28VztqVZS8yhhAUZJc68Ja8VsI90SI81JKjBjis9aIB3doEdd6SBvTyYpKoA==";
    //支付宝公钥
    //  public String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";

    //    public String SELLER = "13758196013";
    public String SELLER = "150054127@qq.com";//

    public String PARTNER = "2088021882351657";
    //   MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALQ9R+aUSvjNIuamAr8edBBVGrZ1Wi8gPUg5NWi9c/8Qk++LDehiLKlNQHI0Rc94pzaPhcVA00VeUoUlRhAS6t0JM3x8dMpR7SgPURiPZHw7gsQJM5ydRXM32b60xzVzj121NsaaWgO1aRwFiBxxQc1X5hc6uLXDh+fybAAoMMuPAgMBAAECgYBFY95dkDP464kHQVyHVeMbMpiRLEbnI8NUHepJA783sJszutZE5B8Loxkvnp5NzMabg8/dbCKqbzDX38zEfXP7tLx3cV3vHuMDvyd0dk3ecHYyrx8/Cydue6Ckt0N3y7oCppDtJ1ixAKSnVqxqQ5J8vr9AmjNSBJ1G4paCJ5fwOQJBAO4PMBSYgP9y7wiDERkQIgT4kyjVsDQb/3rqeKvXrqa7JrAsRVL/EsA4f+B2rPx65O/0iTrHFqhkwD8mG0qI7GUCQQDB0pbCVK1GUiN+mXm4uSM/WbXuW/lFrPixoWXLKdYhtE0XkodIVLKgP7z5iHYu6ZWhbA6UHtS6xoEuccoO9JbjAkEAr4mpdXZioHpagddLk0SPIUU+Ff1D2ZjPkQ41/tiiYzVbOMrcL3AAnpbKzHQV6HKiP8H8Msg9D9yC2c26kAJY9QJAFzaKecG8cuSZfasR6e66avbqlB6Nzyt1KYkD8P4UuDqo1P7mluIV9p1Lm7MdPGcbVZbkdBbxMWFezVoxcfMXMwJBAOwrrKJgzMmWtOSuAp6/Q8swk1nFYdHXPz7tGE/gynlipsxB+8/sTQkTthMgmZu4tU7/LB4YEUp77tus7I/rRZw=
    public String RSA_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALQ9R+aUSvjNIuamAr8edBBVGrZ1Wi8gPUg5NWi9c/8Qk++LDehiLKlNQHI0Rc94pzaPhcVA00VeUoUlRhAS6t0JM3x8dMpR7SgPURiPZHw7gsQJM5ydRXM32b60xzVzj121NsaaWgO1aRwFiBxxQc1X5hc6uLXDh+fybAAoMMuPAgMBAAECgYBFY95dkDP464kHQVyHVeMbMpiRLEbnI8NUHepJA783sJszutZE5B8Loxkvnp5NzMabg8/dbCKqbzDX38zEfXP7tLx3cV3vHuMDvyd0dk3ecHYyrx8/Cydue6Ckt0N3y7oCppDtJ1ixAKSnVqxqQ5J8vr9AmjNSBJ1G4paCJ5fwOQJBAO4PMBSYgP9y7wiDERkQIgT4kyjVsDQb/3rqeKvXrqa7JrAsRVL/EsA4f+B2rPx65O/0iTrHFqhkwD8mG0qI7GUCQQDB0pbCVK1GUiN+mXm4uSM/WbXuW/lFrPixoWXLKdYhtE0XkodIVLKgP7z5iHYu6ZWhbA6UHtS6xoEuccoO9JbjAkEAr4mpdXZioHpagddLk0SPIUU+Ff1D2ZjPkQ41/tiiYzVbOMrcL3AAnpbKzHQV6HKiP8H8Msg9D9yC2c26kAJY9QJAFzaKecG8cuSZfasR6e66avbqlB6Nzyt1KYkD8P4UuDqo1P7mluIV9p1Lm7MdPGcbVZbkdBbxMWFezVoxcfMXMwJBAOwrrKJgzMmWtOSuAp6/Q8swk1nFYdHXPz7tGE/gynlipsxB+8/sTQkTthMgmZu4tU7/LB4YEUp77tus7I/rRZw=";
    public String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";

    private static final int SDK_PAY_FLAG = 1;

    private static final int SDK_CHECK_FLAG = 2;

    private AliPayUtils() {
    }

    private static AliPayUtils aliPayUtils;

    public static AliPayUtils getInstance() {
        if (aliPayUtils == null)
            aliPayUtils = new AliPayUtils();
        return aliPayUtils;
    }

    public interface OnAliPayListener {

        void onError(String error);

        void onPaying();//支付结果确认中

        void onPaySuccess();//支付成功

        void onPayFailed();//支付失败
    }

    private OnAliPayListener listener;
    private Activity activity;

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
                    String resultInfo = payResult.getResult();
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        listener.onPaySuccess();
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            listener.onPaying();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            listener.onPayFailed();
                        }
                    }
                    break;
                }
                case SDK_CHECK_FLAG: {
                    //Toast.makeText(AliPayUtils.this, "检查结果为：" + msg.obj,Toast.LENGTH_SHORT).show();
                    break;
                }
                default:
                    break;
            }
        }
    };

    /**
     * call alipay sdk pay. 调用SDK支付
     */
    /**
     * @param subject 商品名称
     * @param body    商品详情
     * @param price   商品价格
     */
    public AliPayUtils pay(final Activity activity, String subject, String body, String price, OnAliPayListener listener) {
        this.activity = activity;
        this.listener = listener;

        if (TextUtils.isEmpty(PARTNER) || TextUtils.isEmpty(RSA_PRIVATE)
                || TextUtils.isEmpty(SELLER)) {
            listener.onError("需要配置PARTNER | RSA_PRIVATE| SELLER");
            return aliPayUtils;
        }
        // 订单
        String orderInfo = getOrderInfo(subject, body, price);

        // 对订单做RSA 签名
        String sign = sign(orderInfo);

        try {
            // 仅需对sign 做URL编码
            sign = URLEncoder.encode(sign, "UTF-8");
            //java.net.URLEncoder.encode("测试", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            //            e.printStackTrace();
        }

        // 完整的符合支付宝参数规范的订单信息
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&" + getSignType();

        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(activity);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
        return aliPayUtils;
    }

    /**
     * check whether the device has authentication alipay account.
     * 查询终端设备是否存在支付宝认证账户
     */
    private void check(View v) {
        Runnable checkRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask payTask = new PayTask(activity);
                // 调用查询接口，获取查询结果
                boolean isExist = payTask.checkAccountIfExist();

                Message msg = new Message();
                msg.what = SDK_CHECK_FLAG;
                msg.obj = isExist;
                mHandler.sendMessage(msg);
            }
        };

        Thread checkThread = new Thread(checkRunnable);
        checkThread.start();

    }

    /**
     * get the sdk version. 获取SDK版本号
     */
    private void getSDKVersion() {
        PayTask payTask = new PayTask(activity);
        String version = payTask.getVersion();
        Toast.makeText(activity, version, Toast.LENGTH_SHORT).show();
    }

    /**
     * 创建订单信息
     *
     * @param subject 商品名称
     * @param body    商品详情
     * @param price   商品价格
     * @return
     */
    private String getOrderInfo(String subject, String body, String price) {

        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + price + "\"";

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + "http://notify.msp.hk/notify.htm"
                + "\"";

        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";

        // extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
        // orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";

        // 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
        // orderInfo += "&paymethod=\"expressGateway\"";

        return orderInfo;
    }

    /**
     * get the out_trade_no for an order. 生成商户订单号，该值在商户端应保持唯一（可自定义格式规范）
     */
    private String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
                Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);

        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content 待签名订单信息
     */
    private String sign(String content) {
        return SignUtils.sign(content, RSA_PRIVATE);
    }

    /**
     * get the sign type we use. 获取签名方式
     */
    private String getSignType() {
        return "sign_type=\"RSA\"";
    }

}
