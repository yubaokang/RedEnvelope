package com.redenvelope.eventbus;

/**
 * Created by ybk on 2015/11/27.
 * 添加好友是否成功
 */
public class AddFriendsSuccess {
    private boolean isAddSuccess;

    public AddFriendsSuccess(boolean isAddSuccess) {
        this.isAddSuccess = isAddSuccess;
    }

    public boolean isAddSuccess() {
        return isAddSuccess;
    }

    public void setAddSuccess(boolean addSuccess) {
        isAddSuccess = addSuccess;
    }
}
