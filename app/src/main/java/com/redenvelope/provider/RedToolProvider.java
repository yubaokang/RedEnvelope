package com.redenvelope.provider;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;

import com.redenvelope.R;
import com.redenvelope.activity.SendRedActivity_;
import com.redenvelope.constant.IntentConst;
import com.redenvelope.utils.MessageUtils;

import io.rong.imkit.RongContext;
import io.rong.imkit.widget.provider.InputProvider;
import io.rong.imlib.model.Conversation;

/**
 * Created by ybk on 2015/10/2.
 */
public class RedToolProvider extends InputProvider.ExtendProvider {

    private String id;//聊天对象的id，用户，群主等等
    private Conversation.ConversationType conversationType;

    public RedToolProvider(RongContext context, Conversation.ConversationType conversationType, String id) {
        super(context);
        this.conversationType = conversationType;
        this.id = id;
    }

    /**
     * 设置展示的图标
     *
     * @param context
     * @return
     */
    @Override
    public Drawable obtainPluginDrawable(Context context) {
        //R.drawable.de_contacts 红包图标
        return context.getResources().getDrawable(R.mipmap.ic_red);
    }

    /**
     * 设置图标下的title
     *
     * @param context
     * @return
     */
    @Override
    public CharSequence obtainPluginTitle(Context context) {
        //R.string.add_contacts
        return "红包";
    }

    /**
     * click 事件
     *
     * @param view
     */
    @Override
    public void onPluginClick(View view) {
        Intent intent = new Intent(view.getContext(), SendRedActivity_.class);
        intent.putExtra(IntentConst.target_Id, id);
        this.startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            //发送红包界面回调，请求接口，获得红包id
            //------接口请求，返回红包id，然后发送消息-------
            String red_num = data.getStringExtra(IntentConst.red_num);
            String red_sum = data.getStringExtra(IntentConst.red_sum);
            String red_my = data.getStringExtra(IntentConst.red_my);
            String red_content = data.getStringExtra(IntentConst.red_content);
            String red_ID = data.getStringExtra(IntentConst.red_ID);
            MessageUtils.sendMessage(conversationType, id, red_ID, TextUtils.isEmpty(red_content) ? "恭喜发财，大吉大利" : red_content);
        }
    }
}