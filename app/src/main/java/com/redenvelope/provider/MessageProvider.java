package com.redenvelope.provider;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.message.RedMessage;

import io.rong.imkit.model.ProviderTag;
import io.rong.imkit.model.UIMessage;
import io.rong.imkit.widget.provider.IContainerItemProvider;
import io.rong.imlib.model.Message;

/**
 * Created by ybk on 2015/10/2.
 */
@ProviderTag(messageContent = RedMessage.class)
public class MessageProvider extends IContainerItemProvider.MessageProvider<RedMessage> {

    class ViewHolder {
        TextView message;
    }

    @Override
    public View newView(Context context, ViewGroup group) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_red_message, null);
        ViewHolder holder = new ViewHolder();
        holder.message = (TextView) view.findViewById(R.id.textView_red);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, int i, RedMessage redMessage, UIMessage uiMessage) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (uiMessage.getMessageDirection() == Message.MessageDirection.SEND) {//消息方向，自己发送的
            holder.message.setBackgroundResource(io.rong.imkit.R.drawable.rc_ic_bubble_right);
        } else {
            holder.message.setBackgroundResource(io.rong.imkit.R.drawable.rc_ic_bubble_left);
        }
        holder.message.setText(redMessage.getContent());
//        AndroidEmoji.ensure((Spannable) holder.message.getText());//显示消息中的 Emoji 表情。
    }

    @Override
    public Spannable getContentSummary(RedMessage data) {
        return new SpannableString("这是一条自定义消息RedMessage");
    }

    @Override
    public void onItemClick(View view, int i, RedMessage redMessage, UIMessage uiMessage) {
        Log.i("-----", "自定义消息点击");
    }

    @Override
    public void onItemLongClick(View view, int i, RedMessage redMessage, UIMessage uiMessage) {

    }


}