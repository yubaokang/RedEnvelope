package com.redenvelope.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.redenvelope.R;
import com.redenvelope.activity.BaseActivity;
import com.redenvelope.response.FriendsListRes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ybk on 2015/11/16.
 */
public class StartGroupAdapter extends BaseAdapter {
    private Context context;
    private List<FriendsListRes.FriendsLisResFriend> friendsResList;

    public StartGroupAdapter(Context context, List<FriendsListRes.FriendsLisResFriend> friendsResList) {
        this.context = context;
        this.friendsResList = friendsResList;
        listInt = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return friendsResList.size();
    }

    @Override
    public Object getItem(int position) {
        return friendsResList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final FriendsListRes.FriendsLisResFriend friend = friendsResList.get(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_start_group, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textView_name.setText(friend.getNickname());
        ImageLoader.getInstance().displayImage(friend.getPortraitpath(), viewHolder.imageView_header, ((BaseActivity) context).getOptionsRound());
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    listInt.add(friend.getId());
                } else {
                    listInt.remove((Integer) friend.getId());
                }
            }
        });
        return convertView;
    }

    private List<Integer> listInt;

    public List<Integer> getListInt() {
        return listInt;
    }

    class ViewHolder {
        private CheckBox checkBox;
        private ImageView imageView_header;
        private TextView textView_name;

        public ViewHolder(View view) {
            checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            imageView_header = (ImageView) view.findViewById(R.id.imageView_header);
            textView_name = (TextView) view.findViewById(R.id.textView_name);
        }
    }
}
