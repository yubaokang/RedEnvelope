package com.redenvelope.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.redenvelope.R;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.utils.ImageLoaderUtils;

public class ViewHolder {
    private SparseArray<View> mViews;
    private int mPosition;
    private View mConvertView;
    private Context mContext;
    private int mLayoutId;
    private DisplayImageOptions options;
    private DisplayImageOptions optionsRound;

    public ViewHolder(Context context, ViewGroup parent, int layoutId, int position) {
        mContext = context;
        mLayoutId = layoutId;
        this.mPosition = position;
        this.mViews = new SparseArray<>();
        mConvertView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        mConvertView.setTag(this);
        options = ImageLoaderUtils.getDisplayImageOptionDefault(R.mipmap.ic_red_header_big);
        optionsRound = ImageLoaderUtils.getDisplayImageOptionRound(R.mipmap.ic_red_header_big, 1000);
    }

    public static ViewHolder get(Context context, View convertView, ViewGroup parent, int layoutId, int position) {
        if (convertView == null) {
            return new ViewHolder(context, parent, layoutId, position);
        } else {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.mPosition = position;
            return holder;
        }
    }

    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    public View getConvertView() {
        return mConvertView;
    }

    public ViewHolder setText(int viewId, String text) {
        TextView tv = getView(viewId);
        tv.setText(text);
        return this;
    }

    public void setChangeChecked(int radioButtonId) {
        RadioButton radioButton = getView(radioButtonId);
        radioButton.setChecked(!radioButton.isChecked());
    }

    public ViewHolder setOnClickListener(int viewId, View.OnClickListener listener) {
        View view = getView(viewId);
        view.setOnClickListener(listener);
        return this;
    }

    public void setImage(int viewId, String url) {
        ImageLoader.getInstance().displayImage(UrlConst.IMAGE_HEADER + url, (ImageView) getView(viewId), options);
    }

    public void setImageRound(int viewId, String url) {
        ImageLoader.getInstance().displayImage(UrlConst.IMAGE_HEADER + url, (ImageView) getView(viewId), optionsRound);
    }

    public ViewHolder setOnTouchListener(int viewId, View.OnTouchListener listener) {
        View view = getView(viewId);
        view.setOnTouchListener(listener);
        return this;
    }

}
