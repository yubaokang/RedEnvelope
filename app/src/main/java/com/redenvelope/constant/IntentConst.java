package com.redenvelope.constant;

/**
 * Created by ybk on 2015/10/7.
 */
public class IntentConst {

    public static final String red_ID = "red_ID";//红包ID
    public static final String red_num = "red_num";//红包个数
    public static final String red_sum = "red_sum";//红包总金额
    public static final String red_my = "red_my";//自控金额
    public static final String red_content = "red_content";//红包文字内容
    public static final String target_Id = "target_Id";
}
