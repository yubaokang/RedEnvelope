package com.redenvelope.constant;

/**
 * Created by ybk on 2015/9/13.
 */
public class UrlConst {
    //    123.57.206.171 旧
//    121.41.34.44
    public static final String SERVICE_URL = "http://121.41.34.44:8080/hddsc/";
    public static final String IMAGE_HEADER = "http://121.41.34.44:8080/hddsc/image/";
    //http://123.57.206.171:8080/hddsc/redBox/redBoxCreate?num=10&money=100&adjust_money=15&user_id=1&group_id=1
    //http://123.57.206.171:8080/hddsc/redBox/distributeRedBox?user_id=1&redBox_id=1

    //发红包
    public static final String SEND_RED_BOX = "redBox/redBoxCreate";
    public static final String CHECK_RED_BOX = "redBox/checkdistributeRedBox";//查询红包是否抢光
    //http://123.57.206.171:8080/hddsc/redBox/checkdistributeRedBox?user_id=1&redBox_id=1
    //抢红包
    public static final String GET_RED_BOX = "redBox/distributeRedBox";

//    http://123.57.206.171:8080/hddsc/redBox/redBoxCreate?=1&group_id=1&user_id=1&adjust_money=1
//    http://123.57.206.171:8080/hddsc/redBox/redBoxCreate?&=100&adjust_money=15&user_id=1&group_id=1

    //注册
    public static final String REGISTER = "user/addUser";//phone=1111&password=1111&nickname=test&nation=中国&zone=杭州&portrait_data=***;---portrait_data是 上传头像
    //登入
    public static final String LOGIN = "user/login";//登入phone=1111&password=1111

    //验证码
    public static final String GET_CODE = "user/getverify";//phone=11111
    //查询好友
    public static final String SEARCH_FRIEND = "friend/searchfriend";//phone=13858083426
    //添加好友
    public static final String ADD_FRIEND = "friend/addfriend";//user_id=1&friend_id=2
    //获取好友列表
    public static final String FRIEND_LIST = "friend/getfriends";//user_id=2

    //确定添加好友
    public static final String SURE_ADD_FRIEND = "surefriend";//user_id=2&friend_id=1

    //创建群组
    public static final String CREATE_GROUP = "group/addGroup";//group_name=test&user_id=1&grank=1

    //添加用户到群组
    public static final String GROUP_ADD_USER = "group/addUsers";//group_id=1&user_id=1

    //添加支付密码
    public static final String ALI_PAY_PWD = "user/modifypaypassword";//id=1&paypassword=1111

    //根据关键字搜索群 返回 是群的信息
    public static final String SEARCH_GROUP = "group/searchgroups";//groupName=好
    //    {"result":"1","mark":[{"id":1,"groupName":"好","grank":1}],"describe":"获取成功"}

    //用户投诉接口：
    public static final String TOU_SHU = "user/addsug";//content=好哈&user_id=1

}

