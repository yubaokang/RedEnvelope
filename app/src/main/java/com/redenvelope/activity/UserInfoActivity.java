package com.redenvelope.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.redenvelope.R;
import com.redenvelope.response.LoginRes;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_user_info)
public class UserInfoActivity extends BaseActivity {

    @ViewById
    TextView textView_title;

    @ViewById
    ImageView imageView_header;
    @ViewById
    TextView textView_nickname;
    @ViewById
    TextView textView_account;
    @ViewById
    ImageView imageView_erweima;
    @ViewById
    TextView textView_address;
    @ViewById
    TextView textView_sex;
    @ViewById
    TextView textView_country;
    @ViewById
    TextView textView_sdf;//个性签名

    LoginRes.LoginResMark mark;

    @AfterViews
    void init() {
        textView_title.setText("我");
        mark = getUserInfo();
        textView_nickname.setText(mark.getNickname());
        textView_account.setText(mark.getPhone());
        textView_address.setText(mark.getAddress());
        textView_sex.setText(mark.getGender());
        textView_country.setText(mark.getNation());
        textView_sdf.setText(mark.getSignature());
        ImageLoader.getInstance().displayImage(mark.getPortraitUri(), imageView_header);
    }

    @Click({R.id.imageView_back})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
        }
    }
}
