package com.redenvelope.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.redenvelope.R;
import com.redenvelope.fragment.WelcomeFragment_;
import com.redenvelope.utils.SPUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_welcome)
public class WelcomeActivity extends BaseActivity {
    @ViewById
    ViewPager viewPager_main;
    private List<Fragment> list;

    @AfterInject
    void inject() {
        boolean isFirst = (boolean) SPUtils.getParam(this, "isFirst", true);
        if (!isFirst) {
            SplashActivity_.intent(this).start();
        }
    }

    @AfterViews
    void init() {
        list = new ArrayList<>();
        list.add(WelcomeFragment_.builder().index(0).build());
        list.add(WelcomeFragment_.builder().index(1).build());
        list.add(WelcomeFragment_.builder().index(2).build());
        viewPager_main.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), list));
    }

    class MyPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> list;

        public MyPagerAdapter(FragmentManager fm, List<Fragment> list) {
            super(fm);
            this.list = list;
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
