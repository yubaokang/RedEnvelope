package com.redenvelope.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.utils.CheckUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_phone)
public class PhoneActivity extends BaseActivity {

    @ViewById
    TextView textView_title;

    @ViewById
    EditText editText_phone;


    @AfterViews
    void init() {
        textView_title.setText("注册");
    }

    @Click({R.id.imageView_back, R.id.textView_get_code})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_get_code:
                String phone = editText_phone.getText().toString().trim();
                if (!CheckUtils.isMobile(phone)) {
                    ToastUtils.ToastMakeText(this, "请输入正确的手机号");
                    return;
                }
                RegisterActivity_.intent(this).phone(phone).start();
                break;
        }
    }
}