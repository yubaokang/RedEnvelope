package com.redenvelope.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.redenvelope.R;
import com.redenvelope.adapter.CommonAdapter;
import com.redenvelope.adapter.ViewHolder;
import com.redenvelope.constant.TestUserName;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.GroupAddUserRequest;
import com.redenvelope.request.SearchGroupRequest;
import com.redenvelope.response.GroupAddUserRes;
import com.redenvelope.response.SearchGroupRes;
import com.redenvelope.utils.ListUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import io.rong.imkit.RongIM;

@EActivity(R.layout.activity_add_friend)
public class SearchGroupActivity extends BaseActivity {

    @ViewById
    EditText editText_add_phone;

    @ViewById
    ListView listView;

    private CommonAdapter<SearchGroupRes.SearchGroupResMark> adapter;
    private List<SearchGroupRes.SearchGroupResMark> list;
    private String groupId;

    private int grank;//组的等级

    @AfterViews
    void init() {
        list = new ArrayList<>();
        adapter = new CommonAdapter<SearchGroupRes.SearchGroupResMark>(this, list, R.layout.item_search_friend) {
            @Override
            public void convert(ViewHolder holder, final SearchGroupRes.SearchGroupResMark mark) {
                holder.setText(R.id.textView_name, mark.getGroupName());
                holder.setOnClickListener(R.id.textView_add, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        groupId = String.valueOf(mark.getId());
                        addGroup();
                        grank = mark.getGrank();
                    }
                });
            }
        };
        listView.setAdapter(adapter);
    }

    @Click({R.id.imageView_back, R.id.textView_search})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_search://搜索
                searchGroup();
                break;
            default:
                break;
        }
    }

    private void searchGroup() {
        String groupName = editText_add_phone.getText().toString();
        if (TextUtils.isEmpty(groupName)) {
            return;
        }
        SearchGroupRequest request = new SearchGroupRequest();
        request.setGroupName(groupName);
        OkHttpUtils.getInstance().post(UrlConst.SEARCH_GROUP, request, SearchGroupRes.class, this);
    }

    private void addGroup() {
        if (Integer.parseInt(getUserInfo().getRank()) < grank) {
            ToastUtils.ToastMakeText(this, "等级不够");
            return;
        }
        GroupAddUserRequest request = new GroupAddUserRequest();
        request.setGroup_id(groupId);
        request.setUser_id(getUserInfo().getUserId());
        OkHttpUtils.getInstance().post(UrlConst.GROUP_ADD_USER, request, GroupAddUserRes.class, this);
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.SEARCH_GROUP:
                SearchGroupRes searchFriendRes = (SearchGroupRes) responseObject;
                List<SearchGroupRes.SearchGroupResMark> markList = searchFriendRes.getMark();
                if (!ListUtils.isEmpty(markList)) {
                    list.addAll(markList);
                    adapter.notifyDataSetChanged();
                }
                break;
            case UrlConst.GROUP_ADD_USER:
                GroupAddUserRes groupAddUserRes = (GroupAddUserRes) responseObject;
                if (groupAddUserRes.getResult().equals("1")) {
                    RongIM.getInstance().startGroupChat(this, groupId, "群组" + TestUserName.groupId);
                }
                break;
        }
    }
}
