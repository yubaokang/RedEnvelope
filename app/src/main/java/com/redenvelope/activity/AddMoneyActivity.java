package com.redenvelope.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alipay.AliPayUtils;
import com.redenvelope.R;
import com.redenvelope.constant.SpConst;
import com.redenvelope.response.LoginRes;
import com.redenvelope.utils.SPUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_add_money)
public class AddMoneyActivity extends BaseActivity {

    @ViewById
    EditText editText_money;

    @ViewById
    TextView textView_title;

    @AfterViews
    void init() {
        textView_title.setText("零钱充值\n安全支付");
    }

    @Click({R.id.textView_next})
    void click(View view) {
        switch (view.getId()) {
            case R.id.textView_next:
                pay();
                break;
        }
    }

    private void pay() {
        final double money = Double.parseDouble(editText_money.getText().toString().trim());
        if (money < 0.01) {
            ToastUtils.ToastMakeText(this, "金额输入错误");
            return;
        }
        AliPayUtils.getInstance().pay(this, "商品名称", "商品详情", money + "", new AliPayUtils.OnAliPayListener() {
            @Override
            public void onError(String error) {

            }

            @Override
            public void onPaying() {

            }

            @Override
            public void onPaySuccess() {
                LoginRes.LoginResMark mark = getUserInfo();
                mark.setUserPlaymoney(String.valueOf(Double.parseDouble(mark.getUserPlaymoney()) + money));
                SPUtils.saveObjectToJson(AddMoneyActivity.this, SpConst.USERINFO, mark);
                finish();
            }

            @Override
            public void onPayFailed() {

            }
        });
    }
}
