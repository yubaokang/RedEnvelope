package com.redenvelope.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.dialog.RedDialog;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.lintener.MessageListener;
import com.redenvelope.message.RedMessage;
import com.redenvelope.provider.MessageProvider;
import com.redenvelope.provider.RedToolProvider;
import com.redenvelope.request.CheckRedRequest;
import com.redenvelope.request.GetRedDetailRequest;
import com.redenvelope.response.CheckRedRes;
import com.redenvelope.response.CheckRedResMark;
import com.redenvelope.response.GetRedDetailRes;
import com.redenvelope.response.GetRedDetailResMark;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

import io.rong.imkit.RongContext;
import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationFragment;
import io.rong.imkit.widget.provider.CameraInputProvider;
import io.rong.imkit.widget.provider.ImageInputProvider;
import io.rong.imkit.widget.provider.InputProvider;
import io.rong.imkit.widget.provider.LocationInputProvider;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.RichContentMessage;

/**
 * 单聊界面
 */
@EActivity(R.layout.activity_one_chat)
public class ChatActivity extends BaseActivity implements View.OnClickListener {
    /*
     * 目标 Id
     */
    private String mTargetId;
    /**
     * 刚刚创建完讨论组后获得讨论组的id 为targetIds，需要根据 为targetIds 获取 targetId
     */
    private String mTargetIds;

    private ConversationFragment fragment;

    private LayoutInflater inflater;

    private String redId;//点击的红包id

    @AfterViews
    public void init() {
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        getIntentDate(getIntent());
        addMessage();
        /**
         * 设置会话界面操作的监听器。
         */
        MessageListener messageListener = new MessageListener();
        messageListener.setOnMessageClickListener(new MessageListener.OnMessageClickListener() {
            @Override
            public void OnMessageClick(Context context, View view, Message message) {
                if (message.getContent() instanceof RichContentMessage) {
                    redId = ((RichContentMessage) message.getContent()).getExtra();
                    //点击红包消息，查询红包信息
                    queryRedDetail();
                }
            }
        });
        RongIM.setConversationBehaviorListener(messageListener);
    }


    //查询红包信息，然后弹出红包信息框
    private void queryRedDetail() {
        showLoading();
        CheckRedRequest checkRedRequest = new CheckRedRequest();
        checkRedRequest.setUser_id(getUserInfo().getUserId());
        checkRedRequest.setRedBox_id(redId);
        OkHttpUtils.getInstance().post(UrlConst.CHECK_RED_BOX, checkRedRequest, CheckRedRes.class, new OkHttpUtils.OnOkHttpListener() {
            @Override
            public void onOkResponse(BaseResponse responseObject) {
                dismissLoading();
                CheckRedRes checkRedRes = (CheckRedRes) responseObject;
                CheckRedResMark mark = checkRedRes.getMark();
                if (mark == null) {
                    ToastUtils.ToastMakeText(ChatActivity.this, "网络错误");
                    return;
                }
                if (responseObject.getResult().equals("1")) {//1:表示有红包；-1:表示没有红包
                    showDialog(mark.getUser_name(), mark.getNum());
                } else {
                    showDialog("红包被抢光", mark.getNum());
                }
            }
        });
    }

    private RedDialog dialog;
    private View layout;
    private TextView textView_name;//谁发的红包
    private TextView textView_red_num;//发了3个红包，金额随机

    private void showDialog(String name, String num) {
        if (layout == null) {
            layout = inflater.inflate(R.layout.layout_get_red, null);
        }
        if (dialog == null) {
            dialog = new RedDialog(this);
            dialog.addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        layout.findViewById(R.id.imageView_open).setOnClickListener(this);
        layout.findViewById(R.id.textView_close_red).setOnClickListener(this);
        layout.findViewById(R.id.layout_see_other).setOnClickListener(this);
        textView_name = (TextView) layout.findViewById(R.id.textView_name);
        textView_red_num = (TextView) layout.findViewById(R.id.textView_red_num);
        textView_name.setText(name);
        textView_red_num.setText("发了" + num + "个红包，金额随机");
        dialog.show();
    }

    @Click({R.id.imageView_back})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_open:
                //查询红包id，获取抢到的金钱数
                queryRedNum();
                break;
            case R.id.textView_close_red:
                dialog.dismiss();
                break;
            case R.id.layout_see_other://看其他人抢的红包
//                Intent intent = new Intent(ChatActivity.this, RedResultActivity_.class);
//                startActivity(intent);
                queryRedNum();
//                RedResultActivity_.intent(this).start();
                break;
        }
    }

    //获得红包数
    private void queryRedNum() {
        showLoading();
        GetRedDetailRequest redDetailRequest = new GetRedDetailRequest();
        redDetailRequest.setUser_id(getUserInfo().getUserId());
        redDetailRequest.setRedBox_id(redId);
        OkHttpUtils.getInstance().post(UrlConst.GET_RED_BOX, redDetailRequest, GetRedDetailRes.class, new OkHttpUtils.OnOkHttpListener() {
            @Override
            public void onOkResponse(BaseResponse responseObject) {
                dismissLoading();
                GetRedDetailRes getRedDetailRes = (GetRedDetailRes) responseObject;
                GetRedDetailResMark mark = getRedDetailRes.getMark();
                if (Integer.parseInt(mark.getUsednum()) < Integer.parseInt(mark.getTotalnum())) {
                    //是否抢到红包，是：跳转到抢到红包详细界面
                    RedResultActivity_.intent(ChatActivity.this).getRedDetailRes(getRedDetailRes).start();
                    dialog.dismiss();
                } else {
                    ToastUtils.ToastMakeText(ChatActivity.this, "红包已被抢光");
                }
            }
        });
    }

    private Conversation.ConversationType mConversationType;

    @ViewById(R.id.textView_title)
    TextView textView_title;

    /**
     * 展示如何从 Intent 中得到 融云会话页面传递的 Uri7
     */
    private void getIntentDate(Intent intent) {
        mTargetId = intent.getData().getQueryParameter("targetId");
        mTargetIds = intent.getData().getQueryParameter("targetIds");
//        intent.getData().getLastPathSegment();//获得当前会话类型
        mConversationType = Conversation.ConversationType.valueOf(intent.getData().getLastPathSegment().toUpperCase(Locale.getDefault()));
        Log.i("=========", "mTargetId--" + mTargetId + "--mTargetIds-" + mTargetIds + "--mConversationType-" + mConversationType.getName());
        addMore(mConversationType);
        enterFragment(mConversationType, mTargetId);

        if (mConversationType.equals(Conversation.ConversationType.GROUP)) {
            textView_title.setText("群聊");
        } else if (mConversationType.equals(Conversation.ConversationType.PRIVATE)) {
            textView_title.setText("单聊");
        }
    }

    //添加自定义功能
    private void addMore(Conversation.ConversationType mConversationType) {
        //扩展功能自定义
        InputProvider.ExtendProvider[] provider = {
                new RedToolProvider(RongContext.getInstance(), mConversationType, mTargetId),//自定义红包
                new LocationInputProvider(RongContext.getInstance()),//地理位置
                new CameraInputProvider(RongContext.getInstance()),//相机
                new ImageInputProvider(RongContext.getInstance()),//图片
        };
        RongIM.getInstance().resetInputExtensionProvider(mConversationType, provider);
    }

    private void addMessage() {
        RongIM.registerMessageType(RedMessage.class);
        RongIM.registerMessageTemplate(new MessageProvider());
    }

    /**
     * 加载会话页面 ConversationFragment
     *
     * @param mConversationType 会话类型
     * @param mTargetId         目标 Id
     */
    private void enterFragment(Conversation.ConversationType mConversationType, String mTargetId) {
        fragment = (ConversationFragment) getSupportFragmentManager().findFragmentById(R.id.conversation);
        Uri uri = Uri.parse("rong://" + getApplicationInfo().packageName).buildUpon()
                .appendPath("conversation").appendPath(mConversationType.getName().toLowerCase())
                .appendQueryParameter("targetId", mTargetId).build();
        fragment.setUri(uri);
    }
}
