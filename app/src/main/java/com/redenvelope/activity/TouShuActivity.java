package com.redenvelope.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.TouShuRequest;
import com.redenvelope.response.TouShuRes;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_tou_shu)
public class TouShuActivity extends BaseActivity {
    @ViewById
    TextView textView_title;
    @ViewById
    EditText editText_toushu;
    @ViewById
    TextView textView_more;

    @AfterViews
    void init() {
        textView_title.setText("投诉");
        textView_more.setText("提交");
    }

    @Click({R.id.imageView_back, R.id.textView_more})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_more:
                submitTouShu();
                break;
        }
    }

    public void submitTouShu() {
        String content = editText_toushu.getText().toString();
        if (TextUtils.isEmpty(content)) {
            ToastUtils.ToastMakeText(this, "投诉内容不能为空");
            return;
        }
        TouShuRequest request = new TouShuRequest();
        request.setContent(content);
        request.setUser_id(getUserInfo().getUserId());
        OkHttpUtils.getInstance().post(UrlConst.TOU_SHU, request, TouShuRes.class, this);
    }


    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.TOU_SHU:
                TouShuRes touShuRes = (TouShuRes) responseObject;
                if ("1".equals(touShuRes.getResult())) {
                    ToastUtils.ToastMakeText(this, touShuRes.getDescribe());
                    finish();
                } else {
                    ToastUtils.ToastMakeText(this, touShuRes.getDescribe());
                }
                break;
        }
    }
}
