package com.redenvelope.activity;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.constant.TestUserName;
import com.redenvelope.fragment.Fragment1_;
import com.redenvelope.fragment.Fragment2_;
import com.redenvelope.fragment.Fragment3_;
import com.redenvelope.utils.ActivityUtils;
import com.redenvelope.utils.BackgroundUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imlib.model.Conversation;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    private int iconUp[] = {
            R.mipmap.ic_message_up,
            R.mipmap.ic_phone_book_up,
            R.mipmap.ic_find_up,
            R.mipmap.ic_me_up
    };

    private int iconDown[] = {
            R.mipmap.ic_message_down,
            R.mipmap.ic_phone_book_down,
            R.mipmap.ic_find_down,
            R.mipmap.ic_me_down

    };

    //下标个数
    private int INDEX_LENGTH = 4;

    @ViewById(R.id.viewPager_main)
    ViewPager viewPager_main;

    @ViewById(R.id.imageView_main_0)
    ImageView imageView_main_0;

    @ViewById(R.id.textView_main_0)
    TextView textView_main_0;

    @ViewById(R.id.imageView_main_1)
    ImageView imageView_main_1;

    @ViewById(R.id.textView_main_1)
    TextView textView_main_1;

    @ViewById(R.id.imageView_main_2)
    ImageView imageView_main_2;

    @ViewById(R.id.textView_main_2)
    TextView textView_main_2;
    @ViewById(R.id.imageView_main_3)
    ImageView imageView_main_3;

    @ViewById(R.id.textView_main_3)
    TextView textView_main_3;
    private ImageView[] imageViews;
    private TextView[] textViews;

    private List<Fragment> fragmentList = new ArrayList<>();
    private ConversationListFragment conversationListFragment;//会话

    ViewPagerAdapter viewPagerAdapter;

    @ViewById
    TextView textView_userName;

    @ViewById
    TextView textView_title;
    @ViewById
    ImageView imageView_more;

    @AfterViews
    void init() {
        ActivityUtils.getInstance().removeOther();
        textView_userName.setText(TestUserName.USER_NAME);
        initView();
        initTab(0);
        enterFragment();
    }

    private void initView() {
        imageViews = new ImageView[]{imageView_main_0, imageView_main_1, imageView_main_2, imageView_main_3};
        textViews = new TextView[]{textView_main_0, textView_main_1, textView_main_2, textView_main_3};
        conversationListFragment = ConversationListFragment.getInstance();
        fragmentList.add(conversationListFragment);//会话列表
        fragmentList.add(new Fragment1_());
        fragmentList.add(new Fragment2_());
        fragmentList.add(new Fragment3_());
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager_main.setAdapter(viewPagerAdapter);
        viewPager_main.setOffscreenPageLimit(4);
        viewPager_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                initTab(position);
                switch (position) {
                    case 0:
                        textView_title.setText("消息");
                        imageView_more.setVisibility(View.VISIBLE);
                        imageView_more.setImageResource(R.mipmap.ic_more_add);
                        break;
                    case 1:
                        textView_title.setText("通讯录");
                        imageView_more.setVisibility(View.VISIBLE);
                        imageView_more.setImageResource(R.mipmap.ic_more_add_phone);
                        break;
                    case 2:
                        textView_title.setText("发现");
                        imageView_more.setVisibility(View.GONE);
                        break;
                    case 3:
                        textView_title.setText("我");
                        imageView_more.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    /**
     * 加载 会话列表 ConversationListFragment
     */
    private void enterFragment() {
//        ConversationListFragment fragment = (ConversationListFragment) getSupportFragmentManager().findFragmentById(R.id.conversationlist);
        Uri uri = Uri.parse("rong://" + getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist")
                .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话非聚合显示
                .appendQueryParameter(Conversation.ConversationType.GROUP.getName(), "false")//设置群组会话聚合显示
                .appendQueryParameter(Conversation.ConversationType.DISCUSSION.getName(), "false")//设置讨论组会话非聚合显示
                .appendQueryParameter(Conversation.ConversationType.SYSTEM.getName(), "false")//设置系统会话非聚合显示
                .build();
        conversationListFragment.setUri(uri);
    }

    /**
     * @param index 下标
     */
    public void initTab(int index) {
        for (int i = 0; i < INDEX_LENGTH; i++) {
            BackgroundUtils.setBackground(this, imageViews[i], iconUp[i]);
            textViews[i].setTextColor(getResources().getColor(R.color.tab_bg_up));
        }
        BackgroundUtils.setBackground(this, imageViews[index], iconDown[index]);
        textViews[index].setTextColor(getResources().getColor(R.color.button_blue));
        viewPager_main.setCurrentItem(index);
    }

    private View popView;
    private PopupWindow popupWindow;

    //消息右上角弹框
    public void showSelectPop() {
        if (popView == null) {
            popView = LayoutInflater.from(this).inflate(R.layout.pop_add_meaaage, null, false);
            popupWindow = new PopupWindow(popView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            popView.findViewById(R.id.layout_qunliao).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToastUtils.ToastMakeText(MainActivity.this, "发起群聊");
//                    RongIM.getInstance().startGroupChat(MainActivity.this, TestUserName.groupId, "群组" + TestUserName.groupId);
                    StartGroupActivity_.intent(MainActivity.this).start();
                }
            });
            popView.findViewById(R.id.layout_add).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SearchFriendActivity_.intent(MainActivity.this).start();
//                    ToastUtils.ToastMakeText(MainActivity.this, "添加朋友");
//                    if (RongIM.getInstance() != null)
//                        RongIM.getInstance().startPrivateChat(MainActivity.this, "1", "余宝康");
                }
            });
        }
        // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_circle_black));
        popupWindow.showAsDropDown(imageView_more);
    }

    @Click({R.id.imageView_more, R.id.layout_main_0, R.id.layout_main_1, R.id.layout_main_2, R.id.layout_main_3})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_more:
                if (viewPager_main.getCurrentItem() == 0) {
                    showSelectPop();
                } else if (viewPager_main.getCurrentItem() == 1) {
                    SearchGroupActivity_.intent(this).start();
                }
                break;
            case R.id.layout_main_0:
                initTab(0);
                break;
            case R.id.layout_main_1:
                initTab(1);
               /* ArrayList<Group> groups = new ArrayList<>();
                groups.add(new Group("1", "群组1", Uri.parse("http://rongcloud.cn/images/logo.png")));
                groups.add(new Group("2", "群组1", Uri.parse("http://rongcloud.cn/images/logo.png")));
                groups.add(new Group("3", "群组1", Uri.parse("http://rongcloud.cn/images/logo.png")));*/

                /**
                 *同步当前用户的群组信息。
                 *@param groups 需要同步的群组实体。
                 *@param callback 同步状态的回调。
                 */

               /* RongIM.getInstance().getRongIMClient().syncGroup(groups, new RongIMClient.OperationCallback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(RongIMClient.ErrorCode errorCode) {

                    }
                });*/
                ////////////////
//                RongIM.getInstance().startGroupChat(MainActivity.this, TestUserName.groupId, "群组" + TestUserName.groupId);
               /* RongIM.getInstance().getRongIMClient().joinGroup("100", "群组100", new RongIMClient.OperationCallback() {

                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(RongIMClient.ErrorCode errorCode) {
                    }
                });*/

                break;
            case R.id.layout_main_2:
                initTab(2);
//                if (RongIM.getInstance() != null)
//                    RongIM.getInstance().startPrivateChat(this, "1", "余宝康");
                break;
            case R.id.layout_main_3:
                initTab(3);
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        private ViewPagerAdapter(FragmentManager mFragmentManager, List<Fragment> fragmentList) {
            super(mFragmentManager);
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }


}
