package com.redenvelope.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.adapter.StartGroupAdapter;
import com.redenvelope.constant.TestUserName;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.CreateGroupRequest;
import com.redenvelope.request.FriendsListRequest;
import com.redenvelope.request.GroupAddUserRequest;
import com.redenvelope.response.CreateGroupRes;
import com.redenvelope.response.FriendsListRes;
import com.redenvelope.response.GroupAddUserRes;
import com.redenvelope.utils.ListUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import io.rong.imkit.RongIM;

/**
 * 发起群聊
 */
@EActivity(R.layout.activity_start_group)
public class StartGroupActivity extends BaseActivity {

    @ViewById
    TextView textView_more;
    @ViewById
    TextView textView_title;
    @ViewById
    ListView listView;

    private StartGroupAdapter adapter;
    private List<FriendsListRes.FriendsLisResFriend> friendsResList;
    private List<Integer> listUserId;

    @AfterViews
    void init() {
        textView_title.setText("发起群聊");
        textView_more.setText("确定");
        friendsResList = new ArrayList<>();
        listUserId = new ArrayList<>();
        adapter = new StartGroupAdapter(this, friendsResList);
        listView.setAdapter(adapter);
        queryFriends();
    }

    @Click({R.id.imageView_back, R.id.textView_more})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_more:
                showDialog();
                break;
        }
    }

    private void queryFriends() {
        FriendsListRequest request = new FriendsListRequest();
        request.setUser_id(getUserInfo().getUserId());
        OkHttpUtils.getInstance().post(UrlConst.FRIEND_LIST, request, FriendsListRes.class, this);
    }

    //创建群组
    private void createGroup() {
        CreateGroupRequest request = new CreateGroupRequest();
        request.setUser_id(getUserInfo().getUserId());
        request.setGroup_name(gName);
        request.setGrank(gRank);
        OkHttpUtils.getInstance().post(UrlConst.CREATE_GROUP, request, CreateGroupRes.class, this);
    }

    private String gRank;//创建群的时候设置等级
    private String gName;//创建群的时候设置等级

    private void addUserInGroup(String groupId) {
        listUserId = adapter.getListInt();
        if (ListUtils.isEmpty(listUserId)) {
            ToastUtils.ToastMakeText(this, "请选择好友");
            return;
        }
        GroupAddUserRequest request = new GroupAddUserRequest();
        request.setGroup_id(groupId);
        request.setUser_id(getString(listUserId));
        OkHttpUtils.getInstance().post(UrlConst.GROUP_ADD_USER, request, GroupAddUserRes.class, this);
    }

    private String getString(List<Integer> listUserId) {
        StringBuilder sb = new StringBuilder();
        for (Integer userId : listUserId) {
            sb.append(userId);
            sb.append(",");
        }
        sb.append(getUserInfo().getUserId());
        return sb.toString();
    }

    private String groupId;

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.FRIEND_LIST:
                FriendsListRes friendsListRes = (FriendsListRes) responseObject;
                List<FriendsListRes.FriendsLisResFriend> list = friendsListRes.getMark();
                if (!ListUtils.isEmpty(list)) {
                    friendsResList.clear();
                    friendsResList.addAll(list);
                    adapter.notifyDataSetChanged();
                }
                break;
            case UrlConst.CREATE_GROUP://创建群组
                CreateGroupRes createGroupRes = (CreateGroupRes) responseObject;
                CreateGroupRes.CreateGroupResMark mark = createGroupRes.getMark();
                if (mark != null) {
                    groupId = mark.getGroupId();
                    addUserInGroup(groupId);
                }
                break;
            case UrlConst.GROUP_ADD_USER://添加用户到群组
                GroupAddUserRes groupAddUserRes = (GroupAddUserRes) responseObject;
                if (groupAddUserRes.getResult().equals("1")) {
                    Log.i("-------->", groupId);
                    RongIM.getInstance().startGroupChat(this, groupId, "群组" + TestUserName.groupId);
                }
                break;
        }
    }

    public void showDialog() {
        final String ranks[] = {"请选择群等级", "1", "2", "3", "4", "5", "6", "7"};
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, ranks);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_start_group, null);
        final EditText editText_name = (EditText) view.findViewById(R.id.editText_name);
        final Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gRank = ranks[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinner.setAdapter(arrayAdapter);
        new AlertDialog.Builder(this)
                .setTitle("请输入群昵称和等级限制")
                .setView(view)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gName = editText_name.getText().toString();
                        if (TextUtils.isEmpty(gName)) {
                            ToastUtils.ToastMakeText(StartGroupActivity.this, "请输入群名称");
                            return;
                        }
                        if (TextUtils.isEmpty(gRank) || "请选择群等级".equals(gRank)) {
                            ToastUtils.ToastMakeText(StartGroupActivity.this, "请输入群等级");
                            return;
                        }
                        createGroup();
                    }
                })
                .setNegativeButton("取消", null)
                .show();
    }
}
