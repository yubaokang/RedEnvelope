package com.redenvelope.activity;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.adapter.CommonAdapter;
import com.redenvelope.adapter.ViewHolder;
import com.redenvelope.response.GetRedDetailRes;
import com.redenvelope.response.GetRedDetailResList;
import com.redenvelope.response.GetRedDetailResMark;
import com.redenvelope.utils.ImageLoaderUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * 展示红包被抢的详细信息
 */
@EActivity(R.layout.activity_red_result)
public class RedResultActivity extends BaseActivity {

    @ViewById
    TextView textView_title;
    @Extra
    GetRedDetailRes getRedDetailRes;
    private GetRedDetailResMark mark;
    private List<GetRedDetailResList> list;

    @ViewById(R.id.listView_red)
    ListView listView_red;

    @AfterViews
    public void init() {
        textView_title.setText("红包");
        mark = getRedDetailRes.getMark();
        list = getRedDetailRes.getList();

        listView_red.addHeaderView(getHeaderView());
        listView_red.setAdapter(new CommonAdapter<GetRedDetailResList>(this, list, R.layout.item_red_result) {
            @Override
            public void convert(ViewHolder holder, GetRedDetailResList redResultList) {
                holder.setText(R.id.textView_name, redResultList.getUserName());
                holder.setText(R.id.textView_red_num, redResultList.getAllocationMoney() + "元");
                holder.setImageRound(R.id.imageView_header, redResultList.getHeadpic());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private View getHeaderView() {
        View headerView = LayoutInflater.from(this).inflate(R.layout.layout_red_header, null);
        ImageView imageView_user_header = (ImageView) headerView.findViewById(R.id.imageView_user_header);
        TextView textView_red_from = (TextView) headerView.findViewById(R.id.textView_red_from);
        TextView textView_content = (TextView) headerView.findViewById(R.id.textView_content);
        TextView textView_get_red_num = (TextView) headerView.findViewById(R.id.textView_get_red_num);
        TextView textView_other = (TextView) headerView.findViewById(R.id.textView_other);
        ImageLoaderUtils.displayImage(getRedDetailRes.getMark().getHeadpic(), imageView_user_header, getOptionsRound());
        textView_red_from.setText(mark.getUser_name());
//        textView_content.setText(mark.get);
        textView_get_red_num.setText(mark.getUsedmoney());
        //已领取1/2个，共0.01/0.02元
        textView_other.setText("已领取" + mark.getUsednum() + "/" + mark.getTotalnum() + "个,共" + mark.getUsedmoney() + "/" + mark.getTotalmoney() + "元");
        return headerView;
    }

    @Click({R.id.imageView_back})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
        }
    }
}
