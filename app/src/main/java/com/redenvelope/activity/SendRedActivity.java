package com.redenvelope.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jungly.gridpasswordview.GridPasswordView;
import com.redenvelope.R;
import com.redenvelope.constant.IntentConst;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.dialog.BaseDialog;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.SendRedRequest;
import com.redenvelope.response.SendRedRes;
import com.redenvelope.response.SendRedResMark;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_send_red)
public class SendRedActivity extends BaseActivity implements View.OnClickListener {

    @ViewById
    TextView textView_title;
    private String userId;//要发红包的用户id
    private String targetId;//接收红包者的id

    @ViewById(R.id.editText_red_num)
    EditText editText_red_num;//红包个数

    @ViewById(R.id.editText_sum)
    EditText editText_sum;//总金额

    @ViewById(R.id.editText_my_yuan)
    EditText editText_my_yuan;//自控金额

    @ViewById(R.id.editText_content)
    EditText editText_content;//文字内容

    @ViewById(R.id.textView_money)
    TextView textView_money;//红包金钱显示

    @ViewById
    LinearLayout layout_my_money;

    @AfterViews
    public void init() {
        textView_title.setText("发红包");
        if (getUserInfo().getFlag() == null || getUserInfo().getFlag().equals("0")) {
            layout_my_money.setVisibility(View.GONE);
        }
        initIntent();
        editText_sum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                textView_money.setText("￥" + editText_sum.getText());
            }
        });
    }

    private void initIntent() {
        userId = getUserInfo().getUserId();
        if (getIntent().hasExtra(IntentConst.target_Id)) {
            targetId = getIntent().getStringExtra(IntentConst.target_Id);
        }
    }

    @Click({R.id.imageView_back, R.id.textView_change_red_way, R.id.textView_send})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_change_red_way://切换发红包模式
                break;
            case R.id.textView_send://赛钱进红包
                showPayMoneyDialog();
                break;
        }
    }

    private BaseDialog dialogPayMoney;
    private ImageView imageView_way_img;
    private TextView textView_way_name;
    private GridPasswordView pswView;
    private String myMoney;//自控金额
    private String content;//内容
    private String redNum;//红包个数
    private String sum;//总金额

    private void showPayMoneyDialog() {
        if (TextUtils.isEmpty(getUserInfo().getPaypassword())) {
            PayPwdActivity_.intent(this).start();
            return;
        }
        redNum = editText_red_num.getText().toString().trim();
        if (TextUtils.isEmpty(redNum) || redNum.equals("0")) {
            ToastUtils.ToastMakeText(this, "红包个数必须大于1");
            return;
        }
        sum = editText_sum.getText().toString();
        if (TextUtils.isEmpty(sum)) {
            ToastUtils.ToastMakeText(this, "请输入红包金额");
            return;
        }

        if (getUserInfo().getFlag() == null || getUserInfo().getFlag().equals("0")) {
            double aver = Double.parseDouble(sum) / Double.parseDouble(redNum);
            myMoney = String.valueOf(Math.random() * aver * 2);
        } else {
            myMoney = editText_my_yuan.getText().toString();
            if (TextUtils.isEmpty(myMoney)) {
                ToastUtils.ToastMakeText(this, "请输入自控金额");
                return;
            }
        }
        if (Double.parseDouble(sum) <= Double.parseDouble(myMoney)) {
            ToastUtils.ToastMakeText(this, "自控金额必须小于红包总金额");
            return;
        }
        content = editText_content.getText().toString();
        content = TextUtils.isEmpty(content) ? "恭喜发财，大吉大利!" : content;
        if (dialogPayMoney == null) {
            final String finalContent = content;
            dialogPayMoney = new BaseDialog(this, R.layout.dialog_pay_money, new BaseDialog.OnReturnView() {
                @SuppressLint("SetTextI18n")
                @Override
                public void returnView(View view) {
                    view.findViewById(R.id.imageView_pay_close).setOnClickListener(SendRedActivity.this);
                    view.findViewById(R.id.layout_choose_pay_way).setOnClickListener(SendRedActivity.this);
                    TextView textView_content = (TextView) view.findViewById(R.id.textView_content);
                    textView_content.setText(finalContent);
                    TextView textView_red_total = (TextView) view.findViewById(R.id.textView_red_total);
                    textView_red_total.setText("￥" + String.valueOf(sum));
                    imageView_way_img = (ImageView) view.findViewById(R.id.imageView_way_img);
                    textView_way_name = (TextView) view.findViewById(R.id.textView_way_name);
                    pswView = (GridPasswordView) view.findViewById(R.id.pswView);
                    pswView.setOnPasswordChangedListener(new GridPasswordView.OnPasswordChangedListener() {
                        @Override
                        public void onChanged(String psw) {
                        }

                        @Override
                        public void onMaxLength(String psw) {
                            if (psw.equals(getUserInfo().getPaypassword())) {
                                dialogPayMoney.dismiss();
                                queryRedId();
                            } else {
                                ToastUtils.ToastMakeText(SendRedActivity.this, "密码输入错误");
                                pswView.clearPassword();
                            }
                        }
                    });
                }
            });
        }
        dialogPayMoney.show(false);
    }

    private BaseDialog dialogPayWay;

    //选择支付方式
    private void showPayWayDialog() {
        if (dialogPayWay == null) {
            dialogPayWay = new BaseDialog(this, R.layout.dialog_pay_way, new BaseDialog.OnReturnView() {
                @Override
                public void returnView(View view) {
                    view.findViewById(R.id.layout_ling).setOnClickListener(SendRedActivity.this);
                    view.findViewById(R.id.imageView_back_pay_way).setOnClickListener(SendRedActivity.this);
                    view.findViewById(R.id.layout_alipay).setOnClickListener(SendRedActivity.this);
                }
            });
        }
        dialogPayWay.show(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_pay_close:
                dialogPayMoney.dismiss();
                break;
            case R.id.layout_choose_pay_way://选择支付方式
                showPayWayDialog();
                break;
            case R.id.imageView_back_pay_way://
                dialogPayWay.dismiss();
                break;
            case R.id.layout_ling://选择零钱支付
                imageView_way_img.setBackgroundResource(R.mipmap.ic_wallet);
                textView_way_name.setText("零钱");
                dialogPayWay.dismiss();
                break;
            case R.id.layout_alipay://选择支付宝支付
                imageView_way_img.setBackgroundResource(R.mipmap.ic_alipay);
                textView_way_name.setText("支付宝");
                dialogPayWay.dismiss();
                break;
        }
    }

    //发送红包需要先在服务端生成红包id。
    private void queryRedId() {
        showLoading();
        SendRedRequest request = new SendRedRequest();
        request.setUser_id(userId);
        request.setAdjust_money(myMoney);
        request.setGroup_id(targetId);
        request.setMoney(sum);
        request.setNum(redNum);
        request.setText(content);
        OkHttpUtils.getInstance().post(UrlConst.SEND_RED_BOX, request, SendRedRes.class, new OkHttpUtils.OnOkHttpListener() {
            @Override
            public void onOkResponse(BaseResponse responseObject) {
                dismissLoading();
                //获得生成的红包id
                SendRedRes sendRedRes = (SendRedRes) responseObject;
                SendRedResMark mark = sendRedRes.getMark();
                if (mark != null) {
                    Intent intent = new Intent();
                    intent.putExtra(IntentConst.red_num, mark.getNum());
                    intent.putExtra(IntentConst.red_sum, mark.getMoney());
                    intent.putExtra(IntentConst.red_content, mark.getText());
                    intent.putExtra(IntentConst.red_ID, mark.getRedBoxId());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    ToastUtils.ToastMakeText(SendRedActivity.this, "红包发送失败");
                }
            }
        });
    }
}
