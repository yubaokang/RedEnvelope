package com.redenvelope.activity;

import android.os.Bundle;
import android.view.View;

import com.redenvelope.R;
import com.redenvelope.utils.ActivityUtils;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_choose_login_register)
public class ChooseLoginRegisterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtils.getInstance().removeOther();

    }

    @Click({R.id.textView_register, R.id.textView_login})
    void click(View view) {
        switch (view.getId()) {
            case R.id.textView_register:
                PhoneActivity_.intent(this).start();
                break;
            case R.id.textView_login:
                LoginActivity_.intent(this).start();
                break;
        }
    }
}

