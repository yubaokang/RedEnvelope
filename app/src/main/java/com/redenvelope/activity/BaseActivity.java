package com.redenvelope.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.redenvelope.R;
import com.redenvelope.constant.SpConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.response.LoginRes;
import com.redenvelope.utils.ActivityUtils;
import com.redenvelope.utils.ImageLoaderUtils;
import com.redenvelope.utils.LoadingUtils;
import com.redenvelope.utils.SPUtils;

public class BaseActivity extends AppCompatActivity implements OkHttpUtils.OnOkHttpListener {
    private LoadingUtils loadingUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtils.getInstance().addActivity(this);
    }

    public void showLoading() {
        if (loadingUtils == null) {
            loadingUtils = new LoadingUtils(this, true);
        }
        loadingUtils.showLoading();
    }

    /**
     * @param isTouchDismiss 点击外部是否消失
     */
    public void showLoading(boolean isTouchDismiss) {
        if (loadingUtils == null) {
            loadingUtils = new LoadingUtils(this, isTouchDismiss);
        }
        loadingUtils.showLoading();
    }

    public void dismissLoading() {
        if (loadingUtils != null) {
            loadingUtils.dismissLoading();
        }
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
    }

    private DisplayImageOptions optionsRound;

    public DisplayImageOptions getOptionsRound() {
        if (optionsRound == null) {
            optionsRound = ImageLoaderUtils.getDisplayImageOptionRound(R.mipmap.ic_red_header_big, 500);
        }
        return optionsRound;
    }

    public LoginRes.LoginResMark getUserInfo() {
        LoginRes.LoginResMark userInfo = (LoginRes.LoginResMark) SPUtils.getObject(this, SpConst.USERINFO, LoginRes.LoginResMark.class);
        if (userInfo == null) {
            return new LoginRes.LoginResMark();
        }
        return userInfo;
    }

}
