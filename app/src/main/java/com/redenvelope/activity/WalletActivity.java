package com.redenvelope.activity;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.redenvelope.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_wallet)
public class WalletActivity extends BaseActivity {

    @ViewById
    TextView textView_title;

    @ViewById
    TextView textView_money;

    @SuppressLint("SetTextI18n")
    @AfterViews
    void init() {
        textView_title.setText("钱包");
        textView_money.setText("￥" + getUserInfo().getUserPlaymoney());
    }

    @Click({R.id.imageView_back, R.id.textView_add_money, R.id.textView_tixian})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_add_money:
                AddMoneyActivity_.intent(this).start();
                break;
            case R.id.textView_tixian:
                ReflectMoneyActivity_.intent(this).start();
                break;
        }
    }
}
