package com.redenvelope.activity;

import android.view.View;
import android.widget.TextView;

import com.jungly.gridpasswordview.GridPasswordView;
import com.redenvelope.R;
import com.redenvelope.constant.SpConst;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.AddPayPwdRequest;
import com.redenvelope.response.AddPayPwdRes;
import com.redenvelope.response.LoginRes;
import com.redenvelope.utils.SPUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_pay_pwd)
public class PayPwdActivity extends BaseActivity {

    @ViewById
    TextView textView_title;
    @ViewById
    GridPasswordView pswView;

    private String paypassword;

    @AfterViews
    void init() {
        textView_title.setText("支付密码设置");
    }

    @Click({R.id.imageView_back, R.id.textView_ok})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_ok:
                paypassword = pswView.getPassWord();
                //最大长度
                addPayPwd();
                break;
        }
    }

    private void addPayPwd() {
        AddPayPwdRequest request = new AddPayPwdRequest();
        request.setId(getUserInfo().getUserId());
        request.setPaypassword(paypassword);
        OkHttpUtils.getInstance().post(UrlConst.ALI_PAY_PWD, request, AddPayPwdRes.class, this);
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.ALI_PAY_PWD:
                AddPayPwdRes addPayPwdRes = (AddPayPwdRes) responseObject;
                if (addPayPwdRes.getResult().equals("1")) {
                    LoginRes.LoginResMark mark = getUserInfo();
                    mark.setPaypassword(paypassword);
                    SPUtils.saveObjectToJson(this, SpConst.USERINFO, mark);
                    ToastUtils.ToastMakeText(this, getUserInfo().getPaypassword());
                    finish();
                } else {
                    ToastUtils.ToastMakeText(this, "操作失败");
                }
                break;
            default:
                break;
        }
    }
}
