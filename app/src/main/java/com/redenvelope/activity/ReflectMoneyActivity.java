package com.redenvelope.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jungly.gridpasswordview.GridPasswordView;
import com.redenvelope.R;
import com.redenvelope.dialog.BaseDialog;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_reflect_money)
public class ReflectMoneyActivity extends BaseActivity {

    @ViewById
    EditText editText_money;
    @ViewById
    EditText editText_pay_account;

    @ViewById
    TextView textView_title;

    @AfterViews
    void init() {
        textView_title.setText("零钱提现");
    }

    @Click({R.id.textView_next})
    void click(View view) {
        switch (view.getId()) {
            case R.id.textView_next:
                reflectMoney();
                break;
        }
    }

    private void reflectMoney() {
        String account = editText_pay_account.getText().toString();
        if (TextUtils.isEmpty(account)) {
            ToastUtils.ToastMakeText(this, "支付宝账号不能为空");
            return;
        }
        String money = editText_money.getText().toString().trim();
        if (TextUtils.isEmpty(money)) {
            ToastUtils.ToastMakeText(this, "请输入提现金额");
            return;
        }
        showDialog(account, money);
    }

    private BaseDialog dialog;

    private void showDialog(final String account, final String money) {
        dialog = new BaseDialog(this, R.layout.dialog_reflect_money, new BaseDialog.OnReturnView() {
            @Override
            public void returnView(View view) {
                TextView textView_reflect_total = (TextView) view.findViewById(R.id.textView_reflect_total);
                textView_reflect_total.setText("￥" + money);
                final GridPasswordView pswView = (GridPasswordView) view.findViewById(R.id.pswView);
                pswView.setOnPasswordChangedListener(new GridPasswordView.OnPasswordChangedListener() {
                    @Override
                    public void onChanged(String psw) {
                    }

                    @Override
                    public void onMaxLength(String psw) {
                        if (psw.equals(getUserInfo().getPaypassword())) {
                            query(account, money);
                        } else {
                            ToastUtils.ToastMakeText(ReflectMoneyActivity.this, "提现密码错误");
                        }
                    }
                });
                view.findViewById(R.id.imageView_close).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
        dialog.show(false);
    }

    //请求提现接口
    private void query(String account, String money) {
        showLoading();
    }

}
