package com.redenvelope.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.redenvelope.R;
import com.redenvelope.adapter.CommonAdapter;
import com.redenvelope.adapter.ViewHolder;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.eventbus.AddFriendsSuccess;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.AddFriendRequest;
import com.redenvelope.request.SearchFriendRequest;
import com.redenvelope.response.AddFriendRes;
import com.redenvelope.response.SearchFriendRes;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

@EActivity(R.layout.activity_add_friend)
public class SearchFriendActivity extends BaseActivity {

    @ViewById
    EditText editText_add_phone;

    @ViewById
    ListView listView;

    private CommonAdapter<SearchFriendRes.SearchFriendResMark> adapter;
    private List<SearchFriendRes.SearchFriendResMark> list;

    @AfterViews
    void init() {
        list = new ArrayList<>();
        adapter = new CommonAdapter<SearchFriendRes.SearchFriendResMark>(this, list, R.layout.item_search_friend) {
            @Override
            public void convert(ViewHolder holder, final SearchFriendRes.SearchFriendResMark searchFriendResMark) {
                holder.setText(R.id.textView_name, searchFriendResMark.getNickname());
                holder.setImageRound(R.id.imageView_header, searchFriendResMark.getPortraitpath());
                holder.setText(R.id.textView_phone, searchFriendResMark.getPhone());
                holder.setOnClickListener(R.id.textView_add, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addFriend(searchFriendResMark.getId() + "");
                    }
                });
            }
        };
        listView.setAdapter(adapter);
    }

    @Click({R.id.imageView_back, R.id.textView_search})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_search://搜索
                searchFriend();
                break;
            default:
                break;
        }
    }

    private void searchFriend() {
        String phone = editText_add_phone.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            return;
        }
        SearchFriendRequest request = new SearchFriendRequest();
        request.setPhone(phone);
        OkHttpUtils.getInstance().post(UrlConst.SEARCH_FRIEND, request, SearchFriendRes.class, this);
    }

    //添加好友
    private void addFriend(String friendId) {
        AddFriendRequest request = new AddFriendRequest();
        request.setUser_id(getUserInfo().getUserId());
        request.setFriend_id(friendId);
        OkHttpUtils.getInstance().post(UrlConst.ADD_FRIEND, request, AddFriendRes.class, this);
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.SEARCH_FRIEND://搜索好友
                SearchFriendRes searchFriendRes = (SearchFriendRes) responseObject;
                SearchFriendRes.SearchFriendResMark mark = searchFriendRes.getMark();
                if (mark != null) {
                    list.add(mark);
                    adapter.notifyDataSetChanged();
                }
                break;
            case UrlConst.ADD_FRIEND://添加好友
                AddFriendRes addFriendRes = (AddFriendRes) responseObject;
                if (addFriendRes.getResult().equals("1")) {
                    ToastUtils.ToastMakeText(this, "添加好友成功");
                    EventBus.getDefault().post(new AddFriendsSuccess(true));
                    finish();
                } else {
                    ToastUtils.ToastMakeText(this, addFriendRes.getDescribe());
                }
                break;
        }
    }
}
