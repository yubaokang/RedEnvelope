package com.redenvelope.activity;


import android.view.View;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.constant.SpConst;
import com.redenvelope.utils.ActivityUtils;
import com.redenvelope.utils.SPUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_setting)
public class SettingActivity extends BaseActivity {

    @ViewById
    TextView textView_title;

    @AfterViews
    void init() {
        textView_title.setText("设置");
    }

    @Click({R.id.imageView_back, R.id.textView_logout})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_logout:
                SPUtils.remove(this, SpConst.USERINFO);
                ChooseLoginRegisterActivity_.intent(this).start();
                finish();
                break;
        }
    }
}
