package com.redenvelope.activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redenvelope.R;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.GetCodeRequest;
import com.redenvelope.request.RegisterRequest;
import com.redenvelope.response.GetCodeRes;
import com.redenvelope.response.RegisterRes;
import com.redenvelope.utils.BitmapUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import de.greenrobot.event.EventBus;

@EActivity(R.layout.activity_register)
public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    @ViewById
    LinearLayout layout_parent;
    @ViewById
    TextView textView_title;
    @ViewById
    ImageView imageView_header;
    @ViewById
    EditText editText_code;
    @ViewById
    EditText editText_pwd;
    @ViewById
    EditText editText_nickname;
    @ViewById
    EditText editText_country;
    @ViewById
    TextView textView_get_code;
    @Extra
    String phone;
    private TimeCount timeCount;

    @AfterViews
    void init() {
        textView_title.setText("注册");
        getCode();
    }

    @Click({R.id.imageView_back, R.id.frameLayout_upload, R.id.textView_get_code, R.id.textView_register})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.frameLayout_upload:
                showPopWindow(layout_parent);
                break;
            case R.id.textView_get_code:
                getCode();
                break;
            case R.id.textView_register:
                queryRegister();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_dcim:
                openDcim();
                popupWindow.dismiss();
                break;
            case R.id.textView_take:
                takePicture();
                popupWindow.dismiss();
                break;
            case R.id.textView_cancel:
                popupWindow.dismiss();
                break;
        }
    }

    private void queryRegister() {
        if (photo == null) {
            ToastUtils.ToastMakeText(this, "请选择头像");
            return;
        }
        String codeInput = editText_code.getText().toString();
        if (!codeInput.equals(code)) {
            ToastUtils.ToastMakeText(this, "验证码输入错误");
            return;
        }
        String password = editText_pwd.getText().toString();
        if (TextUtils.isEmpty(password)) {
            ToastUtils.ToastMakeText(this, "请输入密码");
            return;
        }
        String nickname = editText_nickname.getText().toString();
        if (TextUtils.isEmpty(nickname)) {
            ToastUtils.ToastMakeText(this, "请输入昵称");
            return;
        }
        String country = editText_country.getText().toString();
        if (TextUtils.isEmpty(country)) {
            ToastUtils.ToastMakeText(this, "请输入国家和地区");
            return;
        }
        RegisterRequest request = new RegisterRequest();
        request.setPhone(phone);
        request.setNation(country);
        request.setZone(country);
        request.setNickname(nickname);
        request.setPassword(password);
        request.setPortrait_data(BitmapUtils.bitmapToBase64(photo));
        OkHttpUtils.getInstance().post(UrlConst.REGISTER, request, RegisterRes.class, this);
    }

    //获取验证码
    private void getCode() {
        GetCodeRequest request = new GetCodeRequest();
        request.setPhone(phone);
        OkHttpUtils.getInstance().post(UrlConst.GET_CODE, request, GetCodeRes.class, this);
    }

    private String code;

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.GET_CODE://获取验证码
                GetCodeRes getCodeRes = (GetCodeRes) responseObject;
                if (!getCodeRes.getResult().equals("1")) {
                    ToastUtils.ToastMakeText(this, getCodeRes.getDescribe());
                    return;
                }
                code = getCodeRes.getMark();
                timeCount = new TimeCount(60 * 1000, 1000);
                timeCount.start();
                break;
            case UrlConst.REGISTER://提交注册信息
                RegisterRes registerRes = (RegisterRes) responseObject;
                if (registerRes.getResult().equals("1")) {
                    LoginActivity_.intent(this).start();
                    finish();
                } else {
                    ToastUtils.ToastMakeText(this, registerRes.getDescribe());
                }
                break;
        }
    }


    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            textView_get_code.setText(millisUntilFinished / 1000 + " s");
            textView_get_code.setClickable(false);
        }

        @Override
        public void onFinish() {
            textView_get_code.setText("没收到");
            textView_get_code.setClickable(true);
        }
    }

    private View popView;
    private PopupWindow popupWindow;
    private RelativeLayout layout_shadow;

    private void showPopWindow(View view) {
        if (popView == null && popupWindow == null) {
            popView = LayoutInflater.from(this).inflate(R.layout.popwindow_alter_header, null, false);

            popupWindow = new PopupWindow(popView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
            // popupWindow.setAnimationStyle(R.style.PopupWindowAnimation2);
            // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
            popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_shadow));

            layout_shadow = (RelativeLayout) popView.findViewById(R.id.layout_shadow);
            TextView textView_dcim = (TextView) popView.findViewById(R.id.textView_dcim);
            TextView textView_take = (TextView) popView.findViewById(R.id.textView_take);
            TextView textView_cancel = (TextView) popView.findViewById(R.id.textView_cancel);
            textView_dcim.setOnClickListener(this);
            textView_take.setOnClickListener(this);
            textView_cancel.setOnClickListener(this);
            layout_shadow.setOnClickListener(this);
        }
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
    }


    private static final int TAKE_PICTURE = 100;
    private static final int FROM_DICM = 200;
    private static final int ZOOM = 300;
    private static String picFileFullName;
    private Bitmap photo;
    private String cut_imgStr = "eHome";

    //打开图库
    private void openDcim() {
        //      Intent intent = new Intent();
        //      intent.setType("image/*"); // 设置文件类型
        //      intent.setAction(Intent.ACTION_GET_CONTENT);
        //4.4
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, FROM_DICM);
    }

    //打开拍照
    public void takePicture() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File outDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!outDir.exists()) {
                outDir.mkdirs();
            }
            File outFile = new File(outDir, System.currentTimeMillis() + ".jpg");
            picFileFullName = outFile.getAbsolutePath();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, TAKE_PICTURE);
        } else {
            Log.e("---", "请确认已经插入SD卡");
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == TAKE_PICTURE) {
                startPhotoZoom(Uri.fromFile(new File(picFileFullName)));
            } else if (requestCode == FROM_DICM) {// 从相册获取图片
                startPhotoZoom(data.getData());
            } else if (requestCode == ZOOM) {// 图片缩放完成
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        photo = extras.getParcelable("data");
                        Drawable drawable = new BitmapDrawable(this.getResources(), photo);
                        imageView_header.setImageDrawable(drawable);
                        saveMyBitmap(cut_imgStr, photo);
                    }
                } else {
                    Log.e("---", "从相册获取图片失败");
                    finish();
                }
            }
        } else {
            // 用户取消了
            finish();
        }
        popupWindow.dismiss();
        super.onActivityResult(requestCode, resultCode, data);
    }

    private File f;// 剪切图保存到文件

    public void saveMyBitmap(String bitName, Bitmap mBitmap) {
        f = new File("/sdcard/" + bitName + ".png");
        Toast.makeText(this, "保存剪裁图片到" + f.getAbsolutePath(),
                Toast.LENGTH_SHORT).show();
        try {
            f.createNewFile();
        } catch (IOException e) {
        }
        FileOutputStream fOut = null;

        try {
            fOut = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);

        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 设置裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 340);
        intent.putExtra("outputY", 340);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, ZOOM);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
