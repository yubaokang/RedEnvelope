package com.redenvelope.activity;

import android.os.Handler;
import android.text.TextUtils;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.redenvelope.R;
import com.redenvelope.utils.ImageLoaderUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends BaseActivity {
    @ViewById
    ImageView imageView_splash;

    @AfterViews
    void init() {
        ImageLoader.getInstance().displayImage("drawable://" + R.drawable.bg_splash_1, imageView_splash, ImageLoaderUtils.getDisplayImageOptionDefault(0));
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getUserInfo() != null && !TextUtils.isEmpty(getUserInfo().getUserId())) {
                    MainActivity_.intent(SplashActivity.this).start();
                } else {
                    ChooseLoginRegisterActivity_.intent(SplashActivity.this).start();
                }
                finish();
            }
        }, 2000);
    }
}
