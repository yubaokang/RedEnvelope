package com.redenvelope.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.redenvelope.R;
import com.redenvelope.constant.SpConst;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.LoginRequest;
import com.redenvelope.response.LoginRes;
import com.redenvelope.utils.CheckUtils;
import com.redenvelope.utils.SPUtils;
import com.redenvelope.utils.ToastUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity {
    @ViewById
    TextView textView_title;

    @ViewById
    EditText editText_phone;

    @ViewById
    EditText editText_pswd;

    @AfterViews
    void init() {
        textView_title.setText("登录");
    }

    @Click({R.id.imageView_back, R.id.textView_register, R.id.textView_login})
    void click(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                finish();
                break;
            case R.id.textView_login:
                login();
                break;
            case R.id.textView_register:
                PhoneActivity_.intent(this).start();
                break;
        }
    }

    private void login() {
        String phone = editText_phone.getText().toString();
        if (!CheckUtils.isMobile(phone)) {
            ToastUtils.ToastMakeText(this, "请输入正确的手机号");
            return;
        }
        String password = editText_pswd.getText().toString();
        if (TextUtils.isEmpty(password)) {
            ToastUtils.ToastMakeText(this, "密码不能为空");
            return;
        }
        LoginRequest request = new LoginRequest();
        request.setPhone(phone);
        request.setPassword(password);
        OkHttpUtils.getInstance().post(UrlConst.LOGIN, request, LoginRes.class, this);
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.LOGIN:
                LoginRes loginRes = (LoginRes) responseObject;
                if (loginRes.getResult().equals("1")) {
                    LoginRes.LoginResMark mark = loginRes.getMark();
                    SPUtils.saveObjectToJson(this, SpConst.USERINFO, mark);
                    MainActivity_.intent(this).start();
                    finish();
                } else {
                    ToastUtils.ToastMakeText(this, loginRes.getDescribe());
                }
                break;
        }
    }
}
