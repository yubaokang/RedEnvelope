package com.redenvelope.utils;

import android.content.Context;
import android.os.Handler;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class SoftKeyboardUtils {

    /**
     * 软键盘显示 隐藏
     *
     * @param editText 触发的输入框
     * @param isShow   true is show
     */
    public static void showHideSoftInput(final EditText editText, boolean isShow) {
        if (isShow) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    editText.requestFocus();
                }
            }, 100);
        } else {
            InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    interface keyBoardListener {
        void onKeyIsShow(boolean isShow);
    }
}
