package com.redenvelope.utils;

import android.util.Log;

import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.message.RichContentMessage;

/**
 * Created by ybk on 2015/10/7.
 */
public class MessageUtils {

    /**
     * 发红包消息
     *
     * @param conversationType 消息类型
     * @param id               用户id，群主id
     * @param content          消息内容
     */
    public static void sendMessage(Conversation.ConversationType conversationType, String id, String redId, String content) {
        RichContentMessage richContentMessage = RichContentMessage.obtain("标题", content, ".");
        richContentMessage.setExtra(redId);//将红包id设置为extra
        /**
         * 发送消息。
         *
         * @param type        会话类型。
         * @param targetId    目标 Id。根据不同的 conversationType，可能是用户 Id、讨论组 Id、群组 Id 或聊天室 Id。
         * @param content     消息内容。
         * @param pushContent push 时提示内容。
         * @param callback    发送消息的回调。
         * @return
         */
        RongIM.getInstance().getRongIMClient().sendMessage(conversationType, id, richContentMessage, "", "", new RongIMClient.SendMessageCallback() {
            @Override
            public void onError(Integer messageId, RongIMClient.ErrorCode e) {
                Log.i("-----", "onError");
            }

            @Override
            public void onSuccess(Integer integer) {
                Log.i("-----", "onSuccess");
            }
        });
    }
}
