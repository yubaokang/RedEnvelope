package com.redenvelope.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by hank on 2015/7/8.
 */

public class SPUtils {
    /**
     * 保存在手机里面的文件名
     */
    private static final String FILE_NAME = "redbox";

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     *
     * @param context
     * @param key
     * @param object
     */

    public static void setParam(Context context, String key, Object object) {
        String type = null;
        try {
            type = object.getClass().getSimpleName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if ("String".equals(type)) {
            editor.putString(key, (String) object);
        } else if ("Integer".equals(type)) {
            editor.putInt(key, (Integer) object);
        } else if ("Boolean".equals(type)) {
            editor.putBoolean(key, (Boolean) object);
        } else if ("Float".equals(type)) {
            editor.putFloat(key, (Float) object);
        } else if ("Long".equals(type)) {
            editor.putLong(key, (Long) object);
        }
        editor.apply();
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param context
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object getParam(Context context, String key, Object defaultObject) {
        String type = null;
        try {
            type = defaultObject.getClass().getSimpleName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        Object object = null;
        if ("String".equals(type)) {
            object = sp.getString(key, (String) defaultObject);
        } else if ("Integer".equals(type)) {
            object = sp.getInt(key, (Integer) defaultObject);
        } else if ("Boolean".equals(type)) {
            object = sp.getBoolean(key, (Boolean) defaultObject);
        } else if ("Float".equals(type)) {
            object = sp.getFloat(key, (Float) defaultObject);
        } else if ("Long".equals(type)) {
            object = sp.getLong(key, (Long) defaultObject);
        }
        return object;
    }

    public void saveObject(Object object) {
    }

    public static boolean isContain(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        if (sp.contains(key)) {
            return true;
        }
        return false;
    }

    public static void saveObjectToJson(Context context, String key, Object object) {
        String json = JSON.toJSONString(object);
        setParam(context, key, json);
    }

    public static Object getObject(Context context, String key, Class clz) {
        String json = getParam(context, key, "").toString();
        if (!json.isEmpty()) {
            return JsonUtil.getObj(json, clz);
        }
        return null;
    }

    /**
     * 将对象转换，保存
     *
     * @param context
     * @param object
     */
    public static void saveObject(Context context, Object object) {
        String json = JSON.toJSONString(object);
        Map<String, String> map = JsonUtil.getMapStr(json);
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object key = entry.getKey();
            Object value = entry.getValue();
            setParam(context, key.toString(), value);
        }
    }

    public static void removeObject(Context context, Class clz) {
        Field[] fields = clz.getDeclaredFields();
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        for (Field field : fields) {
            editor.remove(field.getName());
        }
        editor.apply();
    }

    public static void remove(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        sp.edit().remove(key).apply();
    }
}
