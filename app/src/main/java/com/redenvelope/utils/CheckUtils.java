package com.redenvelope.utils;

import android.app.Activity;
import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 公共校验类
 */
public class CheckUtils {

   private Activity activity;

   public CheckUtils(Activity activity) {
      this.activity = activity;
   }

   /**
    * 校验登录用户名（长度不限）
    *
    * @param username
    * @return true:验证失败
    */
   public static boolean isLoginUsername(String username) {
      if (TextUtils.isEmpty(username)) {
         // PrintUtils.ToastMakeText(activity, "请输入用户名");
         return true;
      }
      if (!username.matches("^[a-zA-Z0-9_\u4E00-\u9FA5]*$")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的用户名");
         return true;
      }
      return false;
   }

   /**
    * 校验用户名
    *
    * @param username
    * @return true:验证失败
    */
   public static boolean isUsername(String username) {
      if (TextUtils.isEmpty(username)) {
         //PrintUtils.ToastMakeText(activity, "请输入用户名");
         return true;
      }
      if (username.matches("^[a-zA-Z0-9_]{1,15}$")) {
         if (username.length() < 4) {
            // PrintUtils.ToastMakeText(activity, "用户名支持4-15位");
            return true;
         }
      } else if (username.matches("^[a-zA-Z0-9_\u4E00-\u9FA5]{1,15}$")) {
         if (username.length() < 2 || username.length() > 7) {
            //PrintUtils.ToastMakeText(activity, "中英文用户名支持2-7位");
            return true;
         }
      } else {
         //PrintUtils.ToastMakeText(activity, "请输入正确的用户名");
         return true;
      }
      return false;
   }

   /**
    * 校验用户密码
    *
    * @param userpwd
    * @return true:验证失败
    */
   public static boolean isUserpwd(String userpwd) {
      if (TextUtils.isEmpty(userpwd)) {
         //PrintUtils.ToastMakeText(activity, "请输入登录密码");
         return true;
      }
      if (userpwd.length() < 6) {
         //PrintUtils.ToastMakeText(activity, "密码支持6-15位");
         return true;
      }
      if (!userpwd.matches("^[a-zA-Z0-9_]{6,15}$")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的登录密码");
         return true;
      }
      return false;
   }

   /**
    * 校验新用户密码
    *
    * @param userpwd
    * @return true:验证失败
    */
   public static boolean isNewUserpwd(String userpwd) {
      if (TextUtils.isEmpty(userpwd)) {
         //PrintUtils.ToastMakeText(activity, "请输入新登录密码");
         return true;
      }
      if (userpwd.length() < 6) {
         //PrintUtils.ToastMakeText(activity, "密码支持6-15位");
         return true;
      }
      if (!userpwd.matches("^[a-zA-Z0-9_]{6,15}$")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的新登录密码");
         return true;
      }
      return false;
   }

   /**
    * 校验重复输入密码
    *
    * @param userpwd
    * @return true:验证失败
    */
   public static boolean isRepeatpwd(String userpwd) {
      if (TextUtils.isEmpty(userpwd)) {
         //PrintUtils.ToastMakeText(activity, "请输入重复密码");
         return true;
      }
      if (userpwd.length() < 6) {
         //PrintUtils.ToastMakeText(activity, "密码支持6-15位");
         return true;
      }
      if (!userpwd.matches("^[a-zA-Z0-9_]{6,15}$")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的重复密码");
         return true;
      }
      return false;
   }

   /**
    * 校验手机号码
    *
    * @param mobileNo
    * @return true:验证失败
    */
   public static boolean isMobile(String mobileNo) {
      Pattern p = Pattern.compile("^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$");
      Matcher m = p.matcher(mobileNo);
      return m.matches();
   }

   /**
    * 校验验证码
    *
    * @param checkcode
    * @return true:验证失败
    */
   public static boolean isCheckCode(String checkcode) {
      if (TextUtils.isEmpty(checkcode)) {
         // PrintUtils.ToastMakeText(activity, "请输入校验码");
         return true;
      }
      if (!checkcode.matches("^[0-9]{4}$")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的校验码");
         return true;
      }
      return false;
   }

   /**
    * 校验真实姓名
    *
    * @param realName
    * @return true:验证失败
    */
   public static boolean isRealname(String realName) {
      if (TextUtils.isEmpty(realName)) {
         //PrintUtils.ToastMakeText(activity, "请输入姓名");
         return true;
      }
      if (!realName.matches("[\u4E00-\u9FA5]{2,4}")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的姓名");
         return true;
      }
      return false;
   }

   /**
    * 校验身份证号码
    *
    * @param idCard
    * @return true:验证失败
    */
   public static boolean isIdCard(String idCard) {
      if (TextUtils.isEmpty(idCard)) {
         //PrintUtils.ToastMakeText(activity, "请输入身份证号码");
         return true;
      }
      if (!idCard.matches("(^\\d{15}$)|(^\\d{17}([0-9]|X)$)")) {
         //PrintUtils.ToastMakeText(activity, "请输入正确的身份证号码");
         return true;
      }
      return false;
   }

   /**
    * 支付宝账户
    *
    * @param username
    * @return true:验证失败
    */
   public static boolean isAlipay(String alipay) {
      if (TextUtils.isEmpty(alipay)) {
         //PrintUtils.ToastMakeText(activity, "请输入支付宝账户");
         return true;
      }
      return false;
   }

   /**
    * 银行账户
    *
    * @param username
    * @return true:验证失败
    */
   public static boolean isBank(String bank) {
      if (TextUtils.isEmpty(bank)) {
         //PrintUtils.ToastMakeText(activity, "请输入银行账号");
         return true;
      }
      return false;
   }

   /**
    * 充值金额
    *
    * @param amount
    * @return
    */
   public static boolean isRechargeAmount(String amount) {
      if (TextUtils.isEmpty(amount)) {
         //PrintUtils.ToastMakeText(activity, "请输入充值金额");
         return true;
      }
      if (Double.parseDouble(amount) < 10) {
         //PrintUtils.ToastMakeText(activity, "充值金额不能小于10元");
         return true;
      }
      return false;
   }
}
