package com.redenvelope.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtils {
    public static void ToastMakeText(Context context, String message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
}
