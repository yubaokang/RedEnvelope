package com.redenvelope.utils;

import java.util.List;

/**
 * Created by ybk on 2015/11/15.
 */
public class ListUtils {
    public static boolean isEmpty(List list) {
        return !(list != null && list.size() > 0);
    }
}
