package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2015/11/15.
 * 获取验证码
 */
public class GetCodeRequest extends BaseRequest {
    private String phone;//手机号

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
