package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2015/10/7.
 * 抢红包请求
 */
public class GetRedDetailRequest extends BaseRequest {
    private String user_id;//用户id
    private String redBox_id;//红包id

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRedBox_id() {
        return redBox_id;
    }

    public void setRedBox_id(String redBox_id) {
        this.redBox_id = redBox_id;
    }
}
