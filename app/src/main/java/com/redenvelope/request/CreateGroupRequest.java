package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2015/11/15.
 * 创建群组
 */
public class CreateGroupRequest extends BaseRequest {
    //    group_name=test&user_id=1;
    private String group_name;
    private String user_id;
    private String grank;//等级

    public String getGroup_name() {
        return group_name;
    }

    public String getGrank() {
        return grank;
    }

    public void setGrank(String grank) {
        this.grank = grank;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
