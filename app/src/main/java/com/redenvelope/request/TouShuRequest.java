package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2016/1/22.
 */
public class TouShuRequest extends BaseRequest {
    private String content;
    private String user_id;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
