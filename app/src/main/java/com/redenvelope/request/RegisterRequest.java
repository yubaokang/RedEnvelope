package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2015/11/15.
 */
public class RegisterRequest extends BaseRequest {
    //    phone=1111&password=1111&nickname=test&nation=中国&zone=杭州&portrait_data=***;---portrait_data是 上传头像
    private String phone;
    private String password;
    private String nickname;//昵称
    private String nation;//国家
    private String zone;//城市
    private String portrait_data;//头像

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getPortrait_data() {
        return portrait_data;
    }

    public void setPortrait_data(String portrait_data) {
        this.portrait_data = portrait_data;
    }
}
