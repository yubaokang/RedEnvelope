package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2015/10/7.
 * 发送红包
 */
public class SendRedRequest extends BaseRequest {
    private String num;//红包个数
    private String money;//红包金额
    private String adjust_money;//自控金额
    private String user_id;//用户id
    private String group_id;//群组id
    private String text;//红包描述

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getAdjust_money() {
        return adjust_money;
    }

    public void setAdjust_money(String adjust_money) {
        this.adjust_money = adjust_money;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }
}
