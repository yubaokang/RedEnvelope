package com.redenvelope.request;

import com.redenvelope.http.BaseRequest;

/**
 * Created by ybk on 2015/11/15.
 * 登入
 */
public class LoginRequest extends BaseRequest {
    private String phone;
    private String password;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
