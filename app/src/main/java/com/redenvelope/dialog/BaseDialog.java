package com.redenvelope.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.redenvelope.R;


/**
 * Created by ybk on 2015/10/10.
 */
public class BaseDialog extends Dialog {

    private View view;

    public BaseDialog(Context context, int layoutId, OnReturnView onReturnView) {
        super(context, R.style.myDialogTheme);
        view = LayoutInflater.from(context).inflate(layoutId, null);
        setContentView(view);
        onReturnView.returnView(view);
    }

    public interface OnReturnView {
        void returnView(View view);
    }

    //是否点击外部消失
    public void show(boolean isTouchDismiss) {
        setCanceledOnTouchOutside(isTouchDismiss);
        show();
    }
}
