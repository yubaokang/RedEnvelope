package com.redenvelope.dialog;

import android.app.Dialog;
import android.content.Context;

import com.redenvelope.R;

/**
 * Created by ybk on 2015/10/7.
 */
public class RedDialog extends Dialog {
    public RedDialog(Context context) {
        super(context, R.style.myDialogTheme);
    }

    public RedDialog(Context context, int theme) {
        super(context, theme);
    }

    protected RedDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
}
