package com.redenvelope.fragment;

import android.net.Uri;
import android.widget.TextView;

import com.redenvelope.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imlib.model.Conversation;

@EFragment(R.layout.fragment_fragment4)
public class Fragment4 extends BaseFragment {

    private TextView textView;

    @AfterViews
    void init() {
        //enterFragment();

       /* textView = (TextView) view.findViewById(R.id.textView);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//**
         *  启动单聊
         *context - 应用上下文。
         *targetUserId - 要与之聊天的用户 Id。
         *title - 聊天的标题，如果传入空值，则默认显示与之聊天的用户名称。
         *//*
                if (RongIM.getInstance() != null) {
                    RongIM.getInstance().startPrivateChat(getActivity(), "2", null);
                }
            }
        });*/
    }

    /**
     * 加载 会话列表 ConversationListFragment
     */
    private void enterFragment() {
        ConversationListFragment fragment = (ConversationListFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.conversationlist);
        Uri uri = Uri.parse("rong://" + getActivity().getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist")
                .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话非聚合显示
                .appendQueryParameter(Conversation.ConversationType.GROUP.getName(), "true")//设置群组会话聚合显示
                .appendQueryParameter(Conversation.ConversationType.DISCUSSION.getName(), "false")//设置讨论组会话非聚合显示
                .appendQueryParameter(Conversation.ConversationType.SYSTEM.getName(), "false")//设置系统会话非聚合显示
                .build();

        fragment.setUri(uri);
    }
}
