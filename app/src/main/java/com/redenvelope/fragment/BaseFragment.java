package com.redenvelope.fragment;

import android.support.v4.app.Fragment;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.redenvelope.R;
import com.redenvelope.constant.SpConst;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.response.LoginRes;
import com.redenvelope.utils.ImageLoaderUtils;
import com.redenvelope.utils.SPUtils;


public class BaseFragment extends Fragment implements OkHttpUtils.OnOkHttpListener {

    public LoginRes.LoginResMark getUserInfo() {
        LoginRes.LoginResMark userInfo = (LoginRes.LoginResMark) SPUtils.getObject(getActivity(), SpConst.USERINFO, LoginRes.LoginResMark.class);
        if (userInfo == null) {
            return new LoginRes.LoginResMark();
        }
        return userInfo;
    }

    private DisplayImageOptions optionsRound;

    public DisplayImageOptions getOptionsRound() {
        if (optionsRound == null) {
            optionsRound = ImageLoaderUtils.getDisplayImageOptionRound(R.mipmap.ic_red_header_big, 5);
        }
        return optionsRound;
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
    }
}
