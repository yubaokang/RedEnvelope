package com.redenvelope.fragment;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.redenvelope.R;
import com.redenvelope.activity.SplashActivity_;
import com.redenvelope.utils.ImageLoaderUtils;
import com.redenvelope.utils.SPUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_welcome)
public class WelcomeFragment extends Fragment {
    @ViewById
    ImageView imageView;
    @ViewById
    TextView textView_in;
    @FragmentArg
    int index;

    private int[] arr = {R.drawable.bg_splash_1, R.drawable.bg_splash_2, R.drawable.bg_splash_3};

    @AfterViews
    void init() {
        if (index == 2) {
            textView_in.setVisibility(View.VISIBLE);
        }
        ImageLoader.getInstance().displayImage("drawable://" + arr[index], imageView, ImageLoaderUtils.getDisplayImageOptionDefault(0));
    }

    @Click({R.id.textView_in})
    void click(View view) {
        switch (view.getId()) {
            case R.id.textView_in:
                SPUtils.setParam(getActivity(), "isFirst", false);
                SplashActivity_.intent(getActivity()).start();
                break;
            default:
                break;
        }
    }
}
