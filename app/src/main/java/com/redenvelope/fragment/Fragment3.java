package com.redenvelope.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.redenvelope.R;
import com.redenvelope.activity.PayPwdActivity_;
import com.redenvelope.activity.SettingActivity_;
import com.redenvelope.activity.TouShuActivity_;
import com.redenvelope.activity.UserInfoActivity_;
import com.redenvelope.activity.WalletActivity_;
import com.redenvelope.response.LoginRes;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_fragment3)
public class Fragment3 extends BaseFragment {

    @ViewById
    TextView textView_name;
    @ViewById
    TextView textView_account;

    @ViewById
    ImageView imageView_me_header;
    private LoginRes.LoginResMark mark;

    @AfterViews
    void init() {
        mark = getUserInfo();
        textView_name.setText(mark.getNickname());
        textView_account.setText(mark.getPhone());
        ImageLoader.getInstance().displayImage(mark.getPortraitUri(), imageView_me_header);
    }

    @Click({R.id.layout_userinfo, R.id.layout_wallet, R.id.layout_pay_pwd, R.id.layout_setting, R.id.layout_toushu})
    void click(View view) {
        switch (view.getId()) {
            case R.id.layout_userinfo:
                UserInfoActivity_.intent(getActivity()).start();
                break;
            case R.id.layout_wallet:
                WalletActivity_.intent(getActivity()).start();
                break;
            case R.id.layout_pay_pwd:
                PayPwdActivity_.intent(getActivity()).start();
                break;
            case R.id.layout_setting:
                SettingActivity_.intent(getActivity()).start();
                break;
            case R.id.layout_toushu:
                TouShuActivity_.intent(getActivity()).start();
                break;
        }
    }
}
