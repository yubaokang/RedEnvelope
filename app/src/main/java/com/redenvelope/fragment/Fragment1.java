package com.redenvelope.fragment;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.redenvelope.R;
import com.redenvelope.adapter.CommonAdapter;
import com.redenvelope.adapter.ViewHolder;
import com.redenvelope.constant.UrlConst;
import com.redenvelope.eventbus.AddFriendsSuccess;
import com.redenvelope.http.BaseResponse;
import com.redenvelope.http.OkHttpUtils;
import com.redenvelope.request.FriendsListRequest;
import com.redenvelope.response.FriendsListRes;
import com.redenvelope.utils.ListUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import io.rong.imkit.RongIM;

@EFragment(R.layout.fragment_fragment1)
public class Fragment1 extends BaseFragment {

    @ViewById
    ListView listView_phone;

    private CommonAdapter<FriendsListRes.FriendsLisResFriend> adapter;
    private List<FriendsListRes.FriendsLisResFriend> friendsResList;

    @AfterInject
    void inject() {
        EventBus.getDefault().register(this);
    }

    @AfterViews
    void init() {
        queryFriends();
        friendsResList = new ArrayList<>();
        adapter = new CommonAdapter<FriendsListRes.FriendsLisResFriend>(getActivity(), friendsResList, R.layout.item_friends) {
            @Override
            public void convert(ViewHolder holder, FriendsListRes.FriendsLisResFriend friend) {
                holder.setImageRound(R.id.imageView_header, friend.getPortraitpath());
                holder.setText(R.id.textView_name, friend.getNickname());
            }
        };
        listView_phone.setAdapter(adapter);
        listView_phone.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FriendsListRes.FriendsLisResFriend friend = friendsResList.get(position);
                RongIM.getInstance().startPrivateChat(getActivity(), friend.getId() + "", friend.getNickname());
            }
        });
    }

    private void queryFriends() {
        FriendsListRequest request = new FriendsListRequest();
        request.setUser_id(getUserInfo().getUserId());
        OkHttpUtils.getInstance().post(UrlConst.FRIEND_LIST, request, FriendsListRes.class, this);
    }

    @Override
    public void onOkResponse(BaseResponse responseObject) {
        switch (responseObject.getUrl()) {
            case UrlConst.FRIEND_LIST:
                FriendsListRes friendsListRes = (FriendsListRes) responseObject;
                List<FriendsListRes.FriendsLisResFriend> list = friendsListRes.getMark();
                if (!ListUtils.isEmpty(list)) {
                    friendsResList.clear();
                    friendsResList.addAll(list);
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    public void onEventMainThread(AddFriendsSuccess addFriendsSuccess) {
        if (addFriendsSuccess.isAddSuccess()) {
            queryFriends();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
