package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

/**
 * Created by ybk on 2015/11/15.
 */
public class GetCodeRes extends BaseResponse {

    /**
     * mark : 2391
     */

    private String mark;

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }
}
