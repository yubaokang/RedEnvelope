package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

import java.util.List;

/**
 * Created by ybk on 2015/11/15.
 */
public class FriendsListRes extends BaseResponse {

    /**
     * id : 3
     * phone : 18200000000
     * userPlaymoney : null
     * nickname : 全是18200000000
     * password : 123456
     * userName : null
     * age : 0
     * portraituri : http://123.57.206.171:8080/hddsc/image/20151115/1115144703-1030.jpg
     * portraitpath : 20151115/1115144703-1030.jpg
     * nation : 中国杭州
     * zone : 中国杭州
     * signature : null
     * address : null
     * gender : null
     */

    private List<FriendsLisResFriend> mark;

    public void setMark(List<FriendsLisResFriend> mark) {
        this.mark = mark;
    }

    public List<FriendsLisResFriend> getMark() {
        return mark;
    }

    public static class FriendsLisResFriend {
        private int id;
        private String phone;
        private Object userPlaymoney;
        private String nickname;
        private String password;
        private Object userName;
        private int age;
        private String portraituri;
        private String portraitpath;
        private String nation;
        private String zone;
        private Object signature;
        private Object address;
        private Object gender;

        public void setId(int id) {
            this.id = id;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setUserPlaymoney(Object userPlaymoney) {
            this.userPlaymoney = userPlaymoney;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public void setUserName(Object userName) {
            this.userName = userName;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void setPortraituri(String portraituri) {
            this.portraituri = portraituri;
        }

        public void setPortraitpath(String portraitpath) {
            this.portraitpath = portraitpath;
        }

        public void setNation(String nation) {
            this.nation = nation;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public void setSignature(Object signature) {
            this.signature = signature;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public int getId() {
            return id;
        }

        public String getPhone() {
            return phone;
        }

        public Object getUserPlaymoney() {
            return userPlaymoney;
        }

        public String getNickname() {
            return nickname;
        }

        public String getPassword() {
            return password;
        }

        public Object getUserName() {
            return userName;
        }

        public int getAge() {
            return age;
        }

        public String getPortraituri() {
            return portraituri;
        }

        public String getPortraitpath() {
            return portraitpath;
        }

        public String getNation() {
            return nation;
        }

        public String getZone() {
            return zone;
        }

        public Object getSignature() {
            return signature;
        }

        public Object getAddress() {
            return address;
        }

        public Object getGender() {
            return gender;
        }
    }
}
