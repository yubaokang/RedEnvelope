package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

import java.util.List;

/**
 * Created by ybk on 2016/1/22.
 */
public class SearchGroupRes extends BaseResponse {

    /**
     * id : 1
     * groupName : 好
     * grank : 1
     */

    private List<SearchGroupResMark> mark;

    public void setMark(List<SearchGroupResMark> mark) {
        this.mark = mark;
    }

    public List<SearchGroupResMark> getMark() {
        return mark;
    }

    public static class SearchGroupResMark {
        private int id;
        private String groupName;
        private int grank;

        public void setId(int id) {
            this.id = id;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public void setGrank(int grank) {
            this.grank = grank;
        }

        public int getId() {
            return id;
        }

        public String getGroupName() {
            return groupName;
        }

        public int getGrank() {
            return grank;
        }
    }
}
