package com.redenvelope.response;

/**
 * Created by ybk on 2015/10/7.
 * 发红包返回该红包的信息
 */
public class SendRedResMark {
    private String groupId;//群组的id'
    private String num;//红包个数
    private String userId;//发红包的用户id
    private String money;//红包金额
    private String redBoxId;//红包id
    private String text;//红包描述

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getRedBoxId() {
        return redBoxId;
    }

    public void setRedBoxId(String redBoxId) {
        this.redBoxId = redBoxId;
    }
}
