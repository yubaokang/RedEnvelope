package com.redenvelope.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ybk on 2015/10/13.
 */
public class GetRedDetailResMark implements Parcelable {
    private String totalmoney;//总金额
    private String user_name;//用户名
    private String usednum;//被抢红包的个数
    private String usedmoney;//被抢金额
    private String totalnum;//红包总数
    private String allocationMoney;//自控金额
    private String headpic;

    public String getHeadpic() {
        return headpic;
    }

    public void setHeadpic(String headpic) {
        this.headpic = headpic;
    }

    public String getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(String totalmoney) {
        this.totalmoney = totalmoney;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUsednum() {
        return usednum;
    }

    public void setUsednum(String usednum) {
        this.usednum = usednum;
    }

    public String getUsedmoney() {
        return usedmoney;
    }

    public void setUsedmoney(String usedmoney) {
        this.usedmoney = usedmoney;
    }

    public String getTotalnum() {
        return totalnum;
    }

    public void setTotalnum(String totalnum) {
        this.totalnum = totalnum;
    }

    public String getAllocationMoney() {
        return allocationMoney;
    }

    public void setAllocationMoney(String allocationMoney) {
        this.allocationMoney = allocationMoney;
    }

    public GetRedDetailResMark() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.totalmoney);
        dest.writeString(this.user_name);
        dest.writeString(this.usednum);
        dest.writeString(this.usedmoney);
        dest.writeString(this.totalnum);
        dest.writeString(this.allocationMoney);
        dest.writeString(this.headpic);
    }

    protected GetRedDetailResMark(Parcel in) {
        this.totalmoney = in.readString();
        this.user_name = in.readString();
        this.usednum = in.readString();
        this.usedmoney = in.readString();
        this.totalnum = in.readString();
        this.allocationMoney = in.readString();
        this.headpic = in.readString();
    }

    public static final Parcelable.Creator<GetRedDetailResMark> CREATOR = new Parcelable.Creator<GetRedDetailResMark>() {
        public GetRedDetailResMark createFromParcel(Parcel source) {
            return new GetRedDetailResMark(source);
        }

        public GetRedDetailResMark[] newArray(int size) {
            return new GetRedDetailResMark[size];
        }
    };
}
