package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

/**
 * Created by ybk on 2015/11/19.
 */
public class AddPayPwdRes extends BaseResponse {

    /**
     * mark : null
     * list : null
     */

    private Object mark;
    private Object list;

    public void setMark(Object mark) {
        this.mark = mark;
    }

    public void setList(Object list) {
        this.list = list;
    }

    public Object getMark() {
        return mark;
    }

    public Object getList() {
        return list;
    }
}
