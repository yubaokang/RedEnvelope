package com.redenvelope.response;

/**
 * Created by ybk on 2015/10/14.
 */
public class CheckRedResMark {
    private String user_name;
    private String num;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
