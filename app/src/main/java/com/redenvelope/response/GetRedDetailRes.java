package com.redenvelope.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.redenvelope.http.BaseResponse;

import java.util.List;

/**
 * Created by ybk on 2015/10/7.
 */
public class GetRedDetailRes extends BaseResponse implements Parcelable {
    private GetRedDetailResMark mark;
    private List<GetRedDetailResList> list;

    public GetRedDetailResMark getMark() {
        return mark;
    }

    public void setMark(GetRedDetailResMark mark) {
        this.mark = mark;
    }

    public List<GetRedDetailResList> getList() {
        return list;
    }

    public void setList(List<GetRedDetailResList> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mark, 0);
        dest.writeTypedList(list);
    }

    public GetRedDetailRes() {
    }

    protected GetRedDetailRes(Parcel in) {
        this.mark = in.readParcelable(GetRedDetailResMark.class.getClassLoader());
        this.list = in.createTypedArrayList(GetRedDetailResList.CREATOR);
    }

    public static final Parcelable.Creator<GetRedDetailRes> CREATOR = new Parcelable.Creator<GetRedDetailRes>() {
        public GetRedDetailRes createFromParcel(Parcel source) {
            return new GetRedDetailRes(source);
        }

        public GetRedDetailRes[] newArray(int size) {
            return new GetRedDetailRes[size];
        }
    };
}
