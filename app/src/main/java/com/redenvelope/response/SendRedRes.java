package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

/**
 * Created by ybk on 2015/10/7.
 * 发红包返回该红包的信息
 */
public class SendRedRes extends BaseResponse {
    private SendRedResMark mark;

    public SendRedResMark getMark() {
        return mark;
    }

    public void setMark(SendRedResMark mark) {
        this.mark = mark;
    }
}
