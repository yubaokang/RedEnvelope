package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

/**
 * Created by ybk on 2016/1/22.
 */
public class TouShuRes extends BaseResponse {
//{"result":"1","mark":null,"describe":"成功"}
    /**
     * mark : null
     */

    private Object mark;

    public void setMark(Object mark) {
        this.mark = mark;
    }

    public Object getMark() {
        return mark;
    }
}
