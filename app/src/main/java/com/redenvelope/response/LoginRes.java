package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

import java.util.List;

/**
 * Created by ybk on 2015/11/15.
 */
public class LoginRes extends BaseResponse {

    /**
     * phone : 18200000000
     * nickname : 全是18200000000
     * password : 123456
     * nation : 中国杭州
     * userPlaymoney : null
     * portraitUri : http://123.57.206.171:8080/hddsc/image/20151115/1115144703-1030.jpg
     * token : mU9t1aIXuL9IfwB1/qumsc9YjIVT1FX8zr2eB7mGHFiVa5cDDrvPiZ/0mVwp5Eh+sRK2nsuIzCY=
     * address : null
     * userId : 3
     * age : 0
     * gender : null
     * userName : null
     * groups : []
     * zone : 中国杭州
     * signature : null
     */

    private LoginResMark mark;
    /**
     * mark : {"phone":"18200000000","nickname":"全是18200000000","password":"123456","nation":"中国杭州","userPlaymoney":"null","portraitUri":"http://123.57.206.171:8080/hddsc/image/20151115/1115144703-1030.jpg","token":"mU9t1aIXuL9IfwB1/qumsc9YjIVT1FX8zr2eB7mGHFiVa5cDDrvPiZ/0mVwp5Eh+sRK2nsuIzCY=","address":null,"userId":"3","age":"0","gender":null,"userName":null,"groups":[],"zone":"中国杭州","signature":null}
     * list : null
     */

    private Object list;

    public void setMark(LoginResMark mark) {
        this.mark = mark;
    }

    public void setList(Object list) {
        this.list = list;
    }

    public LoginResMark getMark() {
        return mark;
    }

    public Object getList() {
        return list;
    }

    public static class LoginResMark {
        private String phone;
        private String nickname;
        private String password;
        private String nation;
        private String rank;
        private String userPlaymoney;
        private String portraitUri;
        private String token;
        private String address;
        private String userId;
        private String age;
        private String gender;
        private String userName;
        private String zone;
        private String signature;
        private List<Integer> groups;
        private String paypassword;
        private String flag;//自控金额--是0的时候表示没有;1的时候-表示有

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getPaypassword() {
            return paypassword;
        }

        public void setPaypassword(String paypassword) {
            this.paypassword = paypassword;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getNation() {
            return nation;
        }

        public void setNation(String nation) {
            this.nation = nation;
        }

        public String getUserPlaymoney() {
            return userPlaymoney;
        }

        public void setUserPlaymoney(String userPlaymoney) {
            this.userPlaymoney = userPlaymoney;
        }

        public String getPortraitUri() {
            return portraitUri;
        }

        public void setPortraitUri(String portraitUri) {
            this.portraitUri = portraitUri;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public List<Integer> getGroups() {
            return groups;
        }

        public void setGroups(List<Integer> groups) {
            this.groups = groups;
        }
    }
}
