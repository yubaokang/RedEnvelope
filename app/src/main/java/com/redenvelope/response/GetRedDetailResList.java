package com.redenvelope.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ybk on 2015/10/13.
 */
public class GetRedDetailResList implements Parcelable {
    private String userId;
    private String userName;//用户名
    private String allocationMoney;//抢到的红包金额
    private String headpic;

    public String getHeadpic() {
        return headpic;
    }

    public void setHeadpic(String headpic) {
        this.headpic = headpic;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAllocationMoney() {
        return allocationMoney;
    }

    public void setAllocationMoney(String allocationMoney) {
        this.allocationMoney = allocationMoney;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.allocationMoney);
        dest.writeString(this.headpic);
    }

    public GetRedDetailResList() {
    }

    protected GetRedDetailResList(Parcel in) {
        this.userId = in.readString();
        this.userName = in.readString();
        this.allocationMoney = in.readString();
        this.headpic = in.readString();
    }

    public static final Parcelable.Creator<GetRedDetailResList> CREATOR = new Parcelable.Creator<GetRedDetailResList>() {
        public GetRedDetailResList createFromParcel(Parcel source) {
            return new GetRedDetailResList(source);
        }

        public GetRedDetailResList[] newArray(int size) {
            return new GetRedDetailResList[size];
        }
    };
}
