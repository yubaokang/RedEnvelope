package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

/**
 * Created by ybk on 2015/11/15.
 */
public class CreateGroupRes extends BaseResponse {

    /**
     * groupId : 1
     * groupName : test
     */

    private CreateGroupResMark mark;
    /**
     * mark : {"groupId":"1","groupName":"test"}
     * list : null
     */

    private Object list;

    public void setMark(CreateGroupResMark mark) {
        this.mark = mark;
    }

    public void setList(Object list) {
        this.list = list;
    }

    public CreateGroupResMark getMark() {
        return mark;
    }

    public Object getList() {
        return list;
    }

    public static class CreateGroupResMark {
        private String groupId;
        private String groupName;

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getGroupId() {
            return groupId;
        }

        public String getGroupName() {
            return groupName;
        }
    }
}
