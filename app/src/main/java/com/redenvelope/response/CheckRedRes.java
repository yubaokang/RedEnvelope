package com.redenvelope.response;

import com.redenvelope.http.BaseResponse;

/**
 * Created by ybk on 2015/10/14.
 */
public class CheckRedRes extends BaseResponse {
    private CheckRedResMark mark;

    public CheckRedResMark getMark() {
        return mark;
    }

    public void setMark(CheckRedResMark mark) {
        this.mark = mark;
    }
}
